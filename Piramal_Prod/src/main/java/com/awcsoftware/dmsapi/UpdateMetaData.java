package com.awcsoftware.dmsapi;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.awcsoftware.dto.UpdateMetaDataRequest;
import com.awcsoftware.dto.UploadResponse;
import com.newgen.dmsapi.DMSXmlResponse;

import ISPack.ISUtil.JPDBRecoverDocData;
import ISPack.ISUtil.JPISIsIndex;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UpdateMetaData {
	final static Logger logger = Logger.getLogger(UpdateMetaData.class);
	@Autowired
	private JPDBRecoverDocData docDBData;
	@Autowired
	private JPISIsIndex newIsIndex;
	@Autowired
	private DMSXmlResponse resp;
//	private CabinetAPI api = new CabinetAPI();
	@Autowired
	private CabinetAPI api;
	@Autowired
	private ApplicationContext ctx;

	public UploadResponse updateMetaData(String sessionId, UpdateMetaDataRequest updateMetaData) {
		UploadResponse ur = ctx.getBean(UploadResponse.class);
		System.out.println(updateMetaData.getMetaDataDictionary().getDocN());
		String inputXml = "<?xml version=\"1.0\"?>\r\n" + "<NGOChangeDocumentProperty_Input>"
				+ "<Option>NGOChangeDocumentProperty</Option>" + "<CabinetName>piramal</CabinetName>" + "<UserDBId>"
				+ sessionId + "</UserDBId>" + "<GroupIndex>0</GroupIndex>" + "<Document>" + "<DocumentIndex>"
				+ updateMetaData.getDocId() + "</DocumentIndex>";
		
		if (updateMetaData.getMetaDataDictionary().getDocN()!=null && !updateMetaData.getMetaDataDictionary().getDocN().isEmpty()) {
			inputXml += "<DocumentName>" + updateMetaData.getMetaDataDictionary().getDocN() + "</DocumentName>";
		}
		inputXml += "<Owner>Supervisor</Owner>" + "<VersionFlag></VersionFlag>" + "<Comment></Comment>"
				+ "<DataDefinition>" + "<DataDefName>document</DataDefName>" + "<Fields>";
		if ((updateMetaData.getMetaDataDictionary().getLeadId()!=null) && !updateMetaData.getMetaDataDictionary().getLeadId().isEmpty()) {

			inputXml += "<Field>" + "<IndexId>20</IndexId>" + "<IndexType>S</IndexType>" + "<IndexValue>"
					+ updateMetaData.getMetaDataDictionary().getLeadId() + "</IndexValue>" + "</Field>";
		}
		if (updateMetaData.getMetaDataDictionary().getDocumentType()!=null && !updateMetaData.getMetaDataDictionary().getDocumentType().isEmpty()) {
			inputXml += "<Field><IndexId>21</IndexId><IndexType>S</IndexType>" + "<IndexValue>"
					+ updateMetaData.getMetaDataDictionary().getDocumentType() + "</IndexValue>" + "</Field>";
		}
		if(updateMetaData.getMetaDataDictionary().getDocument()!=null && !updateMetaData.getMetaDataDictionary().getDocument().isEmpty()) {
		inputXml += "<Field><IndexId>22</IndexId><IndexType>S</IndexType>" + " <IndexValue>"
				+ updateMetaData.getMetaDataDictionary().getDocument() + "</IndexValue></Field>";
		}
		if(updateMetaData.getMetaDataDictionary().getApplicant()!=null && !updateMetaData.getMetaDataDictionary().getApplicant().isEmpty()) {
		inputXml+= "<Field><IndexId>23</IndexId>" + "<IndexType>S</IndexType><IndexValue>"
				+ updateMetaData.getMetaDataDictionary().getApplicant() + "</IndexValue></Field>";
		}
		String inputXml1 = "<?xml version=\"1.0\"?>\r\n" + "<NGOChangeDocumentProperty_Input>"
				+ "<Option>NGOChangeDocumentProperty</Option>" + "<CabinetName>piramal</CabinetName>" + "<UserDBId>"
				+ sessionId + "</UserDBId>" + "    <GroupIndex>0</GroupIndex>" + "<Document>" + "<DocumentIndex>"
				+ updateMetaData.getDocId() + "</DocumentIndex>" + "<DocumentName>"
				+ updateMetaData.getMetaDataDictionary().getDocN() + "</DocumentName>" + "<Owner>Supervisor</Owner>"
				+ "<VersionFlag></VersionFlag>" + "<Comment></Comment>" + "<DataDefinition>"
				+ "<DataDefName>document</DataDefName>" + "<Fields>" + "<Field>" + "<IndexId>20</IndexId>"
				+ "<IndexType>S</IndexType>" + "<IndexValue>" + updateMetaData.getMetaDataDictionary().getLeadId()
				+ "</IndexValue>" + "</Field>" + "<Field><IndexId>21</IndexId><IndexType>S</IndexType>" + "<IndexValue>"
				+ updateMetaData.getMetaDataDictionary().getDocumentType() + "</IndexValue>" + "</Field>"
				+ "<Field><IndexId>22</IndexId><IndexType>S</IndexType>" + " <IndexValue>"
				+ updateMetaData.getMetaDataDictionary().getDocument() + "</IndexValue></Field><Field>"
				+ "<IndexId>23</IndexId>" + "<IndexType>S</IndexType><IndexValue>"
				+ updateMetaData.getMetaDataDictionary().getApplicant() + "</IndexValue></Field>";
		if(updateMetaData.getMetaDataDictionary().getUpdatedFlag() !=null) {
		if (updateMetaData.getMetaDataDictionary().getUpdatedFlag().equalsIgnoreCase("Y")
				|| updateMetaData.getMetaDataDictionary().getUpdatedFlag().equalsIgnoreCase("N")) {
			if (updateMetaData.getCallerSystem().equals("UNO")) {
				inputXml += "<Field><IndexId>40</IndexId><IndexType>S</IndexType><IndexValue>"
						+ updateMetaData.getMetaDataDictionary().getUpdatedFlag() + "</IndexValue></Field>";
			}
			if (updateMetaData.getCallerSystem().equals("SALESFORCE")) {
				inputXml += "<Field><IndexId>63</IndexId><IndexType>S</IndexType><IndexValue>"
						+ updateMetaData.getMetaDataDictionary().getUpdatedFlag() + "</IndexValue></Field>";
			}
			if (updateMetaData.getCallerSystem().equals("PENNANT")) {
				inputXml += "<Field><IndexId>64</IndexId><IndexType>S</IndexType><IndexValue>"
						+ updateMetaData.getMetaDataDictionary().getUpdatedFlag() + "</IndexValue></Field>";
			}
		}}
		if (updateMetaData.getMetaDataDictionary().getCif()!=null && !updateMetaData.getMetaDataDictionary().getCif().isEmpty()) {
			inputXml += "<Field>" + "<IndexId>65</IndexId><IndexType>S</IndexType><IndexValue>"
					+ updateMetaData.getMetaDataDictionary().getCif() + "</IndexValue>" + "</Field>";
		}
		inputXml += " </Fields></DataDefinition><OwnerIndex></OwnerIndex><OwnerType>U</OwnerType></Document></NGOChangeDocumentProperty_Input>";
		logger.info("api= " + api);
		logger.info("inputXml= " + inputXml);
		resp = api.callBroker(inputXml);
		logger.info("resp= " + resp);
		if (resp.getVal("Status").equalsIgnoreCase("0")) {
			logger.info("MetaData updated Successfully");
			String indexParse = resp.getVal("ISIndex");
			indexParse = indexParse.substring(0, indexParse.indexOf("#"));
			ur.setDocId(Integer.parseInt(resp.getVal("DocumentIndex")));
			ur.setStatus(resp.getVal("Status"));
			ur.setImageIndex(Integer.parseInt(indexParse));
		} else {
			logger.info("Incomplete Document Upload Call");
			ur.setDocId(0);
			ur.setImageIndex(0);
			ur.setStatus(resp.getVal("Error"));
		}
		return ur;
	}

}
