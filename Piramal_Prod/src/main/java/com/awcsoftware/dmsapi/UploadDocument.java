package com.awcsoftware.dmsapi;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.awcsoftware.dao.IPiramalDBOperations;
import com.awcsoftware.dto.BaseData;
import com.awcsoftware.dto.CaseData;
import com.awcsoftware.dto.DocumentData;
import com.awcsoftware.dto.UploadResponse;
import com.awcsoftware.services.PropertiesReader;
import com.newgen.dmsapi.DMSXmlResponse;
import ISPack.CPISDocumentTxn;
import ISPack.ISUtil.JPDBRecoverDocData;
import ISPack.ISUtil.JPISException;
import ISPack.ISUtil.JPISIsIndex;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UploadDocument {
	final static Logger logger = Logger.getLogger(UploadDocument.class);
	@Autowired
	private JPDBRecoverDocData docDBData;
	@Autowired
	private JPISIsIndex newIsIndex;
	@Autowired
	private DMSXmlResponse resp;
	@Autowired
	private CabinetAPI api;
	@Autowired
	private ApplicationContext ctx;

	public static String getDocType(String ext) {
		String docType = "";
		if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("JPEG") || ext.equalsIgnoreCase("png")
				|| ext.equalsIgnoreCase("tiff") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("png")) {
			logger.info("Doctype is Image");
			docType = "I";
		} else if (ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("vob") || ext.equalsIgnoreCase("mkv")
				|| ext.equalsIgnoreCase("webm") || ext.equalsIgnoreCase("avi")) {
			logger.info("Doctype is Video");
			docType = "V";
		} else if (ext.equalsIgnoreCase("pdf")) {
			docType = "I";
		} else {
			logger.info("Doctype is Others");
			docType = "N";
		}
		return docType;
	}

	public UploadResponse upload(BaseData bd, String sessionId,File fileX) {

		IPiramalDBOperations dbOps = ctx.getBean(IPiramalDBOperations.class);
		UploadResponse ur = ctx.getBean(UploadResponse.class);
		String docType = null;
		int docIndex = 0;
		String isIndex = null;
		String location = null;
		
		int folderIndex = 0;
		String dd = null;
		char salesforce='N',pennant='N',uno = 'N';
		if (bd instanceof DocumentData) {
			DocumentData ddX = (DocumentData) bd;
			folderIndex = dbOps.validateCreateFolder(dbOps.validateLocation(ddX.getLocation()), bd, sessionId);
			location = ddX.getLocation();
			if(ddX.getDocSource().equalsIgnoreCase("SALESFORCE")) {
				salesforce='Y';
			}else if(ddX.getDocSource().equalsIgnoreCase("PENNANT")) {
				pennant='Y';
			}else if(ddX.getDocSource().equalsIgnoreCase("UNO")) {
				uno='Y';
			}else if(ddX.getDocSource().equalsIgnoreCase("OMNISCAN")){
				salesforce='N';pennant='N';uno='N';
			}
			

			dd = "<DataDefinition>" + 
					"<DataDefIndex>3</DataDefIndex><DataDefName>document</DataDefName><Fields>" + 
					"<Field>" + 
					"<IndexId>9</IndexId><IndexName>leadId</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getLeadId()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>10</IndexId><IndexName>documentType</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getDocumentType()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>11</IndexId><IndexName>document</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getDocument()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>12</IndexId><IndexName>applicant</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getApplicant()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>13</IndexId><IndexName>userUploaded</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getUserName()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>14</IndexId><IndexName>original</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>N</IndexValue><pickable>Y</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>15</IndexId><IndexName>UNOIntegration</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+uno+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>16</IndexId><IndexName>MetaDataUpdate</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>Y</IndexValue><pickable>Y</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>17</IndexId><IndexName>Branch</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+location.toUpperCase()+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>" + 
					"<IndexId>1018</IndexId><IndexName>docSource</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getDocSource()+"</IndexValue><pickable>N</pickable></Field>" + 
					"</Fields>" + 
					"<Field>" + 
					"<IndexId>1019</IndexId><IndexName>SalesForceIntegration</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+salesforce+"</IndexValue><pickable>N</pickable></Field>" + 
					"</Fields>" + 
					"<Field>" + 
					"<IndexId>1020</IndexId><IndexName>PennantIntegration</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+pennant+"</IndexValue><pickable>N</pickable></Field>" + 
					"<Field>"+
					"<IndexId>1021</IndexId><IndexName>cif</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+ddX.getCif()+"</IndexValue><Pickable>N</Pickable></Field>"+
					"</Fields>" + 
					"</DataDefinition>";
		}

		else {
			CaseData cdX = (CaseData) bd;
			folderIndex = dbOps.validateCreateFolder(
					dbOps.validateLocation(PropertiesReader.getProp().getProperty("cm")), bd, sessionId);
			dd = "<DataDefinition>"
					+ "<DataDefIndex>4</DataDefIndex>"
					+ "<Fields>"
					+ "<Field>"
					+ "<IndexId>18</IndexId><IndexName>leadId</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexFlag>D</IndexFlag><Pickable>N</Pickable><IndexValue>"+cdX.getLeadId()+"</IndexValue>"
					+ "</Field>"
					+ "<Field>"
					+ "<IndexId>19</IndexId><IndexName>caseId</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexFlag>D</IndexFlag><Pickable>N</Pickable><IndexValue>"+cdX.getCaseId()+"</IndexValue>"
					+ "</Field>"
					+ "<Field>"
					+ "<IndexId>20</IndexId><IndexName>documentType</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexFlag>D</IndexFlag><Pickable>N</Pickable><IndexValue>"+cdX.getDocumentType()+"</IndexValue>"
					+ "</Field>"
					+ "<Field>"
					+ "<IndexId>21</IndexId><IndexName>document</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexFlag>D</IndexFlag><Pickable>N</Pickable><IndexValue>"+cdX.getDocument()+"</IndexValue>"
					+ "</Field>"
					+ "</Fields>"
					+ "<DataDefName>case</DataDefName>"
					+ "</DataDefinition>";
		}

		if (folderIndex == -1 || folderIndex == 0) {
			ur.setDocId(0);
			ur.setImageIndex(0);
			ur.setStatus("Please contact Admin, Some Issues with location root folder");
			return ur;
		} else {
			try {
				logger.info("Tracking Folder Index :" + folderIndex);

				docType = getDocType(FilenameUtils.getExtension(fileX.getName()));
				int pageNum = 1;
				logger.info("Before pdf check, file extension is: "+FilenameUtils.getExtension(fileX.getName()));
				if (FilenameUtils.getExtension(fileX.getName()).equals("pdf")) {
					logger.info("PDF check true");
					PDDocument doc = PDDocument.load(fileX);
					logger.info("PDF Loaded");
					pageNum = doc.getNumberOfPages();
					logger.info("PDF number of pages:"+pageNum);
					doc.close();
				}
				
				logger.info("Number of pages before OD call:"+pageNum);
				
				String xmlData = "<?xml version=\"1.0\"?><NGOAddDocument_Input><Option>NGOAddDocument</Option><CabinetName>"
						+ PropertiesReader.getProp().getProperty("cabinetName") + "</CabinetName><UserDBId>" + sessionId
						+ "</UserDBId><GroupIndex></GroupIndex><Document><ParentFolderIndex>" + folderIndex
						+ "</ParentFolderIndex><NoOfPages>" + pageNum
						+ "</NoOfPages><AccessType>S</AccessType><DocumentName>" + FilenameUtils.getBaseName(fileX.getName())
						+ "</DocumentName><CreationDateTime></CreationDateTime><ExpiryDateTime></ExpiryDateTime><VersionFlag>N</VersionFlag><DocumentType>"
						+ docType + "</DocumentType><DocumentSize>" + fileX.length()
						+ "</DocumentSize><CreatedByApp></CreatedByApp><CreatedByAppName>" + FilenameUtils.getExtension(fileX.getName())
						+ "</CreatedByAppName><ISIndex></ISIndex><FTSDocumentIndex>0</FTSDocumentIndex><ODMADocumentIndex></ODMADocumentIndex><Comment></Comment><Author></Author><OwnerIndex>1</OwnerIndex><EnableLog>Y</EnableLog><FTSFlag>PP</FTSFlag>"
						+ dd + "<NameLength>255</NameLength></Document></NGOAddDocument_Input>";

				CPISDocumentTxn.AddDocument_MT(null, PropertiesReader.getProp().getProperty("serverIp"),
						Short.parseShort(PropertiesReader.getProp().getProperty("jtsPort")),
						PropertiesReader.getProp().getProperty("cabinetName"), Short.parseShort("1"),
						PropertiesReader.getProp().getProperty("fileLocation") + fileX.getName(), docDBData, "1",
						newIsIndex);
				docIndex = newIsIndex.m_nDocIndex;
				isIndex = newIsIndex.m_nDocIndex + "#" + newIsIndex.m_sVolumeId;
				logger.info("DocIndex :" + docIndex + " /isIndex :" + isIndex);
				String frontData = xmlData.substring(0, xmlData.indexOf("<ISIndex>") + 9);
				String endData = xmlData.substring(xmlData.indexOf("</ISIndex>"), xmlData.length());
				xmlData = frontData + isIndex + endData;

				resp = api.callBroker(xmlData);

				if (resp.getVal("Status").equalsIgnoreCase("0")) {
					logger.info("Completed Document Upload Call");
					String indexParse = resp.getVal("ISIndex");
					indexParse = indexParse.substring(0, indexParse.indexOf("#"));
					ur.setDocId(Integer.parseInt(resp.getVal("DocumentIndex")));
					ur.setStatus(resp.getVal("Status"));
					ur.setImageIndex(Integer.parseInt(indexParse));
				} else {
					logger.info("Incomplete Document Upload Call");
					ur.setDocId(0);
					ur.setImageIndex(0);
					ur.setStatus(resp.getVal("Error"));

				}
				logger.info("File Delete Operation : " + fileX.delete());

			} catch (IOException | JPISException e) {
				logger.error(e);
			}
			return ur;
		}

	}

}