package com.awcsoftware.dmsapi;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.awcsoftware.dao.impl.PiramalDBOperationsImpl;
import com.awcsoftware.dto.UserData;
import com.awcsoftware.exceptions.PiramalException;
import com.awcsoftware.services.PropertiesReader;
import com.newgen.dmsapi.DMSCallBroker;
import com.newgen.dmsapi.DMSInputXml;
import com.newgen.dmsapi.DMSXmlResponse;

@Component
public class CabinetAPI {
	final static Logger logger = Logger.getLogger(CabinetAPI.class);
	@Autowired
	private DMSInputXml inputxml;
	@Autowired
	private DMSXmlResponse response;
//	String outputxml;

	/*
	 * Main Call for All DMS API
	 */
	DMSXmlResponse callBroker(String str) {
		try {
			response.setXmlString(DMSCallBroker.execute(str, PropertiesReader.getProp().getProperty("serverIp"),
					Integer.valueOf(PropertiesReader.getProp().getProperty("jtsPort")), 0));
//			logger.info("response after= "+response);
		} catch (Exception e) {
			logger.info("Error in DMS Call Broker"+e);
//			throw new PiramalException("DMS operartion was unsuccessful, Please check your inputs first");
		}
		
		return response;
	}

	public DMSXmlResponse connectCabinet(UserData ud) {
		return callBroker(inputxml.getConnectCabinetXml(PropertiesReader.getProp().getProperty("cabinetName"),
				ud.getUsername(), ud.getUserpassword(), "", "Y", "0", "S", ""));
	}

	public DMSXmlResponse disconnectCabinet(String sessionId) {
		return callBroker(
				inputxml.getDisconnectCabinetXml(PropertiesReader.getProp().getProperty("cabinetName"), sessionId));
	}

	public DMSXmlResponse createFolder(int parentFolderIndex, String folderName, String sessionId) {
		return callBroker(inputxml.getAddFolderXml(PropertiesReader.getProp().getProperty("cabinetName"), sessionId,
				String.valueOf(parentFolderIndex), folderName, "", "", "1", "", "", "", "N", "", "", "", "", "", "",
				"Y", "", "255", "0", "0", "", ""));
	}

	public DMSXmlResponse getDocumentProperty(String sessionId, int docId) {
		return callBroker(inputxml.getGetDocumentPropertyXml(PropertiesReader.getProp().getProperty("cabinetName"),
				sessionId, "", String.valueOf(docId), "Y", ""));
	}

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		CabinetAPI api = ctx.getBean(CabinetAPI.class);
		
		DMSXmlResponse resp = api
				.callBroker(api.inputxml.getGetDocumentPropertyXml("piramal", "-541874538", "", "18984", "Y", ""));
		System.out.println("resp= "+resp);
		Map<String, String> fieldMap = new HashMap<>();
		fieldMap.put("Field_20", "y");
		System.out.println(fieldMap.isEmpty());

		/*
		 * try { DocumentBuilderFactory dbFactory =
		 * DocumentBuilderFactory.newInstance(); DocumentBuilder builder =
		 * dbFactory.newDocumentBuilder(); InputSource is= new InputSource(new
		 * StringReader(resp.toString())); Document doc = builder.parse(is);
		 * doc.getDocumentElement().normalize(); System.out.println("Root element :" +
		 * doc.getDocumentElement().getNodeName());
		 * System.out.println(doc.getElementsByTagName("DataDefinition")); NodeList
		 * flowList = doc.getElementsByTagName("DataDefinition"); String rdate=null;
		 * System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"); for (int i = 0; i
		 * < flowList.getLength(); i++) { NodeList childList =
		 * flowList.item(i).getChildNodes(); for (int j = 0; j < childList.getLength();
		 * j++) { Node childNode = childList.item(j); // if
		 * ("RevisedDateTime".equals(childNode.getNodeName())) {
		 * System.out.println(childList.item(j).getTextContent() .trim());
		 * rdate=childList.item(j).getTextContent().trim(); if(rdate.contains("63")) {
		 * System.out.println("*"+rdate+"*"); } // } } } // NodeList flowList =
		 * doc.getElementsByTagName("Document"); // String rdate=null; //
		 * System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"); // for (int i =
		 * 0; i < flowList.getLength(); i++) { // NodeList childList =
		 * flowList.item(i).getChildNodes(); // for (int j = 0; j <
		 * childList.getLength(); j++) { // Node childNode = childList.item(j); // if
		 * ("RevisedDateTime".equals(childNode.getNodeName())) { //
		 * System.out.println(childList.item(j).getTextContent() // .trim()); //
		 * rdate=childList.item(j).getTextContent().trim(); // } // } // } //
		 * System.out.println(rdate); // java.sql.Timestamp ts1 =
		 * java.sql.Timestamp.valueOf( rdate ) ; // System.out.println("ts1= "+ts1 ); //
		 * java.sql.Timestamp ts2 =
		 * java.sql.Timestamp.valueOf("2020-05-26 13:16:01.087") ; //
		 * System.out.println("ts2= "+ts2 ); // // if(ts1.after(ts2)) { //
		 * System.out.println("ts1  is After ts2"); // }else { //
		 * System.out.println("ts1 is not after ts2"); // }
		 * 
		 * System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 */
//		if (resp.getVal("Status").equalsIgnoreCase("0")) {
//			output = resp.getVal("UserDBId");
//		} else
//			output = resp.getVal("Error");
		PiramalDBOperationsImpl pdbo=new PiramalDBOperationsImpl();
		System.out.println("*************************************");
//		List<Integer> docIds=pdbo.getDocIdFromLeadIdFilterByDoctypes("2017090500003","KYC","INCOME");
//		System.out.println(docIds);
		System.out.println("*************************************");
		
//		System.out.println(api.getDocumentProperty("1362418509", 18968)); 
		JSONObject soapDatainJsonObject = XML.toJSONObject(api.getDocumentProperty("-541874538", 18984).toString());
		System.out.println(soapDatainJsonObject);

//		System.out.println("docSource= "+soapDatainJsonObject.get("docSource")); 
	}
}
