package com.awcsoftware.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.awcsoftware.dto.ISIndexData;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ISIndexMapper implements RowMapper<ISIndexData> {

	@Override
	public ISIndexData mapRow(ResultSet rs, int rowNum) throws SQLException {
		ISIndexData isd=new ISIndexData();
		isd.setDocumentIndex(rs.getInt("documentIndex"));
		isd.setName(rs.getString("Name"));
		isd.setImageIndex(rs.getInt("ImageIndex"));
		isd.setVolumeId(rs.getInt("VolumeId"));
		isd.setParentFolderIndex(rs.getInt("ParentFolderIndex"));
		return isd;
	}

}
