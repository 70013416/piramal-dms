package com.awcsoftware.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.awcsoftware.dto.VersionContent;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VersionContentMapper implements RowMapper<VersionContent> {

	@Override
	public VersionContent mapRow(ResultSet rs, int rowNum) throws SQLException {
		VersionContent vc=new VersionContent();
		vc.setDocumentId(rs.getInt("DocumentIndex"));
		vc.setVersion(rs.getFloat("VersionNumber"));
		vc.setVersionComment(rs.getString("VersionComment"));
		vc.setImageIndex(rs.getInt("ImageIndex"));
		return vc;
	}

}
