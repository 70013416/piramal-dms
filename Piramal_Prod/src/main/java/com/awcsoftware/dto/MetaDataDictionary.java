package com.awcsoftware.dto;

import java.io.Serializable;

public class MetaDataDictionary implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String leadId;
	String cif;
	String documentType;
	String document;
	String applicant;
	String docN;
	String documentClass;
	String updatedFlag;
	
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getDocN() {
		return docN;
	}
	public void setDocN(String docN) {
		this.docN = docN;
	}
	public String getDocumentClass() {
		return documentClass;
	}
	public void setDocumentClass(String documentClass) {
		this.documentClass = documentClass;
	}
	
	public String getUpdatedFlag() {
		return updatedFlag;
	}
	public void setUpdatedFlag(String updatedFlag) {
		this.updatedFlag = updatedFlag;
	}
	@Override
	public String toString() {
		return "MetaDataDictionary [leadId=" + leadId + ", cif=" + cif + ", documentType=" + documentType
				+ ", document=" + document + ", applicant=" + applicant + ", docN=" + docN + ", documentClass="
				+ documentClass + ", updatedFlag=" + updatedFlag + "]";
	}
	
}
