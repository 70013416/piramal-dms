package com.awcsoftware.dto;

import java.io.Serializable;

public class UpdateMetaDataRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String docId;
	String imageIndex;
	String callerSystem;
	MetaDataDictionary metaDataDictionary;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getImageIndex() {
		return imageIndex;
	}

	public void setImageIndex(String imageIndex) {
		this.imageIndex = imageIndex;
	}

	public String getCallerSystem() {
		return callerSystem;
	}

	public void setCallerSystem(String callerSystem) {
		this.callerSystem = callerSystem;
	}

	public MetaDataDictionary getMetaDataDictionary() {
		return metaDataDictionary;
	}

	public void setMetaDataDictionary(MetaDataDictionary metaDataDictionary) {
		this.metaDataDictionary = metaDataDictionary;
	}

	@Override
	public String toString() {
		return "UpdateMetaDataRequest [docId=" + docId + ", imageIndex=" + imageIndex + ", callerSystem=" + callerSystem
				+ ", metaDataDictionary=" + metaDataDictionary + "]";
	}

//	@Override
//	public String toString() {
//		return "UpdateMetaDataRequest [docId=" + docId + ", imageIndex=" + imageIndex +  ", callerSystem=" + callerSystem +"]"
//				+"MetaDataDictionary [leadId=" + metaDataDictionary.getLeadId() + ", cif=" + metaDataDictionary.getCif() + ", documentType=" + metaDataDictionary.getDocumentType()
//				+ ", document=" + metaDataDictionary.getDocument() + ", applicant=" + metaDataDictionary.getApplicant() + ", docN=" + metaDataDictionary.getDocN() + ", documentClass="
//				+ metaDataDictionary.getDocumentType() + ", updatedFlag=" + metaDataDictionary.getUpdatedFlag() + "]";
//	}
}
