package com.awcsoftware.dto;

public class ISIndexData {
	private int documentIndex,imageIndex,volumeId,parentFolderIndex;
	private String name;
	public ISIndexData() {
	}
	public ISIndexData(int documentIndex, int imageIndex, int volumeId, int parentFolderIndex, String name) {
		super();
		this.documentIndex = documentIndex;
		this.imageIndex = imageIndex;
		this.volumeId = volumeId;
		this.parentFolderIndex = parentFolderIndex;
		this.name = name;
	}
	public int getDocumentIndex() {
		return documentIndex;
	}
	public void setDocumentIndex(int documentIndex) {
		this.documentIndex = documentIndex;
	}
	public int getImageIndex() {
		return imageIndex;
	}
	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}
	public int getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(int volumeId) {
		this.volumeId = volumeId;
	}
	public int getParentFolderIndex() {
		return parentFolderIndex;
	}
	public void setParentFolderIndex(int parentFolderIndex) {
		this.parentFolderIndex = parentFolderIndex;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + documentIndex;
		result = prime * result + imageIndex;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + parentFolderIndex;
		result = prime * result + volumeId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ISIndexData other = (ISIndexData) obj;
		if (documentIndex != other.documentIndex)
			return false;
		if (imageIndex != other.imageIndex)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentFolderIndex != other.parentFolderIndex)
			return false;
		if (volumeId != other.volumeId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ISIndexData [documentIndex=" + documentIndex + ", imageIndex=" + imageIndex + ", volumeId=" + volumeId
				+ ", parentFolderIndex=" + parentFolderIndex + ", name=" + name + "]";
	}
	
}
