package com.awcsoftware.dto;

public class RootFolder {
	private String name;
	private int folderIndex;
	public RootFolder() {
	}
	public RootFolder(String name, int folderIndex) {
		super();
		this.name = name;
		this.folderIndex = folderIndex;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getFolderIndex() {
		return folderIndex;
	}
	public void setFolderIndex(int folderIndex) {
		this.folderIndex = folderIndex;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + folderIndex;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RootFolder other = (RootFolder) obj;
		if (folderIndex != other.folderIndex)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RootFolder [name=" + name + ", folderIndex=" + folderIndex + "]";
	}
		
}
