package com.awcsoftware.dao.impl;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.model.KeysAndAttributes;
import com.awcsoftware.dao.IPiramalDBOperations;
import com.awcsoftware.dmsapi.CabinetAPI;
import com.awcsoftware.dto.BaseData;
import com.awcsoftware.dto.CaseData;
import com.awcsoftware.dto.DocumentData;
import com.awcsoftware.dto.GeneralFolder;
import com.awcsoftware.dto.ISIndexData;
import com.awcsoftware.dto.RootFolder;
import com.awcsoftware.dto.VersionContent;
import com.awcsoftware.dto.mapper.GeneralFolderMapper;
import com.awcsoftware.dto.mapper.ISIndexMapper;
import com.awcsoftware.dto.mapper.RootFolderMapper;
import com.awcsoftware.dto.mapper.VersionContentMapper;
import com.awcsoftware.services.PropertiesReader;
import com.awcsoftware.validations.DMSCallerSystem;
import com.newgen.dmsapi.DMSXmlResponse;

@Component
public class PiramalDBOperationsImpl implements IPiramalDBOperations {
	final static Logger logger = Logger.getLogger(PiramalDBOperationsImpl.class);
	private static String[] locations;
	private static String suffix;
	private static String blank;
	static {
		blank="";
		suffix="Checker-Approve";
		locations=PropertiesReader.getProp().getProperty("locations").split(",");
	}
	@Autowired
	private JdbcTemplate template;
	
	@Autowired
	private ApplicationContext ctx;
	
	@Override
	public VersionContent getVersionData(int docId) {
		Object[] obj = new Object[] {docId};
		try {
		List<VersionContent> vc=template.query(PropertiesReader.getProp().getProperty("versionContentQuery"), obj, ctx.getBean(VersionContentMapper.class));
		return vc.get(0);
		}
		catch(IndexOutOfBoundsException e) {
			VersionContent vc=new VersionContent();
			return vc;
		}
		
	}
	
	
	@Override
	public String validateSessionId(String sessionId) {
		Object[] obj=new Object[] {sessionId};
		try {
		template.queryForObject(PropertiesReader.getProp().getProperty("sessionIdQuery"), obj, Integer.class);
		return "true";
		}
		catch(EmptyResultDataAccessException e) {
			return "Invalid Session Id";
		}
	}
	@Override
	public String validateLocation(String location) {
		if(location.equalsIgnoreCase("CaseManagement")) {
			return "CaseManagement";
		}
		else {
			for(String temp:locations) {
				if(temp.equalsIgnoreCase(location)) {
					return temp+suffix;
				}
			}
			return blank;
		}
	}
	
	@Override
	public int validateCreateFolder(String locationData, BaseData dd, String sesisonId) {
		return rootFolder(locationData,dd, sesisonId);
	}
	public int rootFolder(String locationData,BaseData dd, String sessionId) {
		int data = 0;
		Object[] obj=new Object[] { locationData };
		
		List<RootFolder> rfList=template.query(PropertiesReader.getProp().getProperty("rootFolderQuery"), obj, ctx.getBean(RootFolderMapper.class));
			if(rfList.size()==1) {
				RootFolder rf=rfList.get(0);
				data = leadIdFolder(rf.getFolderIndex(),dd, sessionId);
			}
			else if(rfList.size()>1) {
				data=-1;
			}
			logger.info("locationData : "+locationData);
			logger.info("data : "+data);
			logger.info("rfList : "+rfList);
		return data;
	}
	
	public int leadIdFolder(int parentFolderIndex,BaseData bd, String sessionId) {
		int data = 0;
		int flag=0;
		String queryData=null;
		if(bd instanceof DocumentData) {
			DocumentData dd=(DocumentData)bd;
			queryData=dd.getLeadId();
		}
		else {
			CaseData cd=(CaseData)bd;
			queryData=cd.getCaseId();
		}
		CabinetAPI api=ctx.getBean(CabinetAPI.class);
		Object[] obj=new Object[] { parentFolderIndex };
		List<GeneralFolder> gfList=template.query(PropertiesReader.getProp().getProperty("generalFolderIdQuery"), obj, ctx.getBean(GeneralFolderMapper.class));
		for(GeneralFolder gf:gfList) {
			if(gf.getName().equals(queryData)) {
				flag++;
				data=gf.getFolderIndex();
				break;
			}
		}
		if(flag==0) {
			DMSXmlResponse resp=api.createFolder(parentFolderIndex, queryData, sessionId);
			data=Integer.parseInt(resp.getVal("FolderIndex"));
		}
		return data;
	}
	
	
	public List<ISIndexData> getIsIndex(int docId) {
		Object[] obj=new Object[] { docId };
		List<ISIndexData> data=template.query(PropertiesReader.getProp().getProperty("isIndexQuery"), obj, ctx.getBean(ISIndexMapper.class));
		return data;
	}


	@Override
	public List<Integer> getDocIdFromLeadId(String leadId) {
		Object[] obj=new Object[] {leadId};
		List<Integer> list=template.queryForList(PropertiesReader.getProp().getProperty("documetLeadQuery"),obj, Integer.class);
		return list;
	}
	


	@Override
	public List<Integer> getDocIdFromLeadIdFilterByDoctypes(String leadId, String[] props,Map<String, String> fieldMap) {
		logger.info("Entered getDocIdFromLeadIdFilterByDoctypes()");
		StringBuilder query=new StringBuilder(PropertiesReader.getProp().getProperty("documetLeadQueryFilterByDoctypes"));
		//AND ( Field_21= 'INCOME' OR  Field_21= 'KYC' OR  Field_21= 'Property' OR  Field_21= 'OTHERS') AND Field_40='Y'
		Object[] obj=new Object[] {leadId};
		for(int i=0;i<props.length;i++) {
			query.append(" Field_21='"+props[i]+"'");
			if(i>=0 && i<props.length-1) {
				query.append(" OR ");
			}
			if(i==props.length-1) {
			query.append(")");
			}
		}
//		Object[] obj=new Object[] {leadId,props1,props2};
		if(!fieldMap.isEmpty()) {
			 Map.Entry<String,String> entry = fieldMap.entrySet().iterator().next();
			 String key = entry.getKey();
			 String value = entry.getValue();
			 logger.info("key= "+key+" value= "+value);
			query.append(" AND "+key+ "="+"'"+value+"'");
			}
		logger.info("query= "+query);

		List<Integer> list=template.queryForList(query.toString(),obj, Integer.class);
		logger.info("doc ids= "+list);
		return list;
	}
	public String validatecallerSystem(String callerSystem) {
		logger.info("Entered validatecallerSystem method. callerSystem= " + callerSystem);
		try {
			DMSCallerSystem dcs = DMSCallerSystem.valueOf(callerSystem);
			return dcs.getCallerSystem();
		} catch (Exception e) {
			logger.debug("Exception occured= "+e);
			return "Invalid CallerSystem";
		}
	}

}
