package com.awcsoftware.exceptions;



import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(annotations = Validated.class)
public class PiramalExceptionHandler extends ResponseEntityExceptionHandler {
	final static Logger LOGGER = Logger.getLogger(PiramalExceptionHandler.class);

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleSpecificException(Exception ex, WebRequest request) {
		logger.info("Entered handleSpecificException(m)");
		String errorMessageDescription = ex.getLocalizedMessage();
		LOGGER.debug("errorMessageDescription= "+errorMessageDescription);
		if (errorMessageDescription == null) {
			errorMessageDescription = ex.toString();
		}
		if(errorMessageDescription.equals("Invalid CallerSystem")) {
			PiramalApiResponse errorMessage = new PiramalApiResponse(LocalDateTime.now(), errorMessageDescription, 400);
			return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}

		PiramalApiResponse errorMessage = new PiramalApiResponse(LocalDateTime.now(), errorMessageDescription, 500);
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
	
	