package com.awcsoftware.validations;

/**
 * List of Caller Systems exists for the DMS to be called till Date.
 * @author S.M.Shuaib
 */
public enum DMSCallerSystem {
		SALESFORCE("SALESFORCE"), PENNANT("PENNANT"), UNO("UNO");
		
		private String callerSystem;
		
		DMSCallerSystem(String callerSystem) {
			this.callerSystem = callerSystem;
		}

		public String getCallerSystem() {
			return callerSystem;
		}
}
