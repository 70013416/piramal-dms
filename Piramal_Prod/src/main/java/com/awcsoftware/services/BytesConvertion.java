/**
 * 
 */
package com.awcsoftware.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BytesConvertion {
	final static Logger logger = Logger.getLogger(BytesConvertion.class);
	
	public BytesConvertion() {
	}

	public byte[] convertToByteData(String filePath) {
		byte[] bytesArray = null;
		try {
			File file = null;
			file = new File(filePath);
			bytesArray = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray);
			fis.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bytesArray;
	}

	public File writeByteArraysToFile(String fileName, byte[] content) throws IOException {
		logger.info("File Writing Started at :"+fileName);
		File file=null;
		file = new File(fileName);
		FileOutputStream fos=new FileOutputStream(file);
		BufferedOutputStream writer = new BufferedOutputStream(fos);
		writer.write(content);
		writer.flush();
		writer.close();
		fos.close();
		logger.info("File Writing Ended at :"+fileName);
		return file;
	}
}
