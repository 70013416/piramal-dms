package com.awcsoftware.services;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.awcsoftware.base64.EncryptionPiramal;
import com.awcsoftware.dao.IPiramalDBOperations;
import com.awcsoftware.dmsapi.CabinetAPI;
import com.awcsoftware.dmsapi.DocumentVersioning;
import com.awcsoftware.dmsapi.DownloadDocument;
import com.awcsoftware.dmsapi.UpdateMetaData;
import com.awcsoftware.dmsapi.UploadDocument;
import com.awcsoftware.dto.BinaryData;
import com.awcsoftware.dto.CaseData;
import com.awcsoftware.dto.DocumentData;
import com.awcsoftware.dto.ISIndexData;
import com.awcsoftware.dto.MetaDataRequest;
import com.awcsoftware.dto.UpdateMetaDataRequest;
import com.awcsoftware.dto.UploadResponse;
import com.awcsoftware.dto.UserData;
import com.awcsoftware.dto.VersionContent;
import com.awcsoftware.dto.VersionRequest;
import com.awcsoftware.dto.VersionResponse;
import com.awcsoftware.exceptions.PiramalException;
import com.newgen.dmsapi.DMSXmlResponse;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OmnidocsServices {
	final static Logger logger = Logger.getLogger(OmnidocsServices.class);
	private String emptyLocation = "Invalid Location or DocSource";
	private String caseManagmentX = "CaseId or Lead Id Can't be empty";
	private String blankCommentsX = "please put some comments";
	private String invalidDocumentId = ", invalid document id";

	@Autowired
	private Tika tika;

	@Autowired
	private ApplicationContext ctx;

	@Autowired
	private EncryptionPiramal em;

	@Autowired
	private BytesConvertion bc;

	private static LinkedList<String> maliciousList;
	static {
		maliciousList = new LinkedList<String>();
		//maliciousList.add("application/zip");
		//maliciousList.add("application/x-zip-compressed");
		maliciousList.add("application/x-sh");
		maliciousList.add("application/x-msi");
		maliciousList.add("application/x-msdownload");
		maliciousList.add("application/x-msdos-program");
		maliciousList.add("application/x-apple-diskimage");
		maliciousList.add("application/vnd");
		maliciousList.add("application/octet-stream");
		maliciousList.add("application/msword");
	}

	public VersionContent getDocumentIndex(int docId) {
		IPiramalDBOperations ops = ctx.getBean(IPiramalDBOperations.class);
		return ops.getVersionData(docId);
	}

	public String getSessionId(UserData ud) {
		CabinetAPI api = ctx.getBean(CabinetAPI.class);
		DMSXmlResponse response = api.connectCabinet(ud);
		if (response.getVal("Status").equalsIgnoreCase("0")) {
			return response.getVal("UserDBId");
		} else {
			return response.getVal("Error");
		}
	}

	public String validateSessionId(String sessionId) {
		try {
			Integer.parseInt(sessionId);
			IPiramalDBOperations ops = ctx.getBean(IPiramalDBOperations.class);
			return ops.validateSessionId(sessionId);
		} catch (NumberFormatException e) {
			return "not a valid data-type for sessionId";
		}
	}

	public boolean isMaliciousItem(File file) {
		boolean flag = false;
		try {
			if (maliciousList.contains(tika.detect(file))) {
				flag = true;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return flag;
	}

	public List<UploadResponse> uploadDocument(ArrayList<DocumentData> dd, String sessionId) {
		logger.info("dd= " + dd);
		ArrayList<UploadResponse> responseList = (ArrayList) ctx.getBean("responseList");
		IPiramalDBOperations ops = ctx.getBean(IPiramalDBOperations.class);
		CabinetAPI api = ctx.getBean(CabinetAPI.class);
		// Check 1 for Session
		String dataX = validateSessionId(sessionId);
		// Check 2 for blank location
		String emptyLocation = "";
		
		for (DocumentData ddx : dd) {
			// if(ddx.getLocation() == null ||
			// ops.validateLocation(ddx.getLocation()).equals("") ||
			// ddx.getDocSource() ==
			// null || ddx.getDocSource().equals("")) {
			if (ddx.getLocation() == null || ops.validateLocation(ddx.getLocation()).equals("")) {
				emptyLocation = this.emptyLocation;
				break;
			}
		}

		List<File> fileList = (ArrayList) ctx.getBean("fileList");
		boolean malicious = false;
		for (DocumentData ddX : dd) {
			try {
				File tempFile = bc
						.writeByteArraysToFile(
								PropertiesReader.getProp().getProperty("fileLocation") + ddX.getLeadId() + " "
										+ ddX.getDocumentType() + " " + ddX.getDocument() + " " + ddX.getApplicant()
										+ "." + FilenameUtils.getExtension(ddX.getDocN()),
								em.decodeData(ddX.getBinData()));
				if (isMaliciousItem(tempFile)) {
					malicious = true;
					break;
				} else {
					fileList.add(tempFile);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			

		}
		try {
			if (dataX.equalsIgnoreCase("true") && !(emptyLocation.equals(this.emptyLocation)) && malicious == false) {
				UploadDocument ud = ctx.getBean(UploadDocument.class);
				for (int i = 0; i < dd.size(); i++) {
					responseList.add(ud.upload(dd.get(i), sessionId, fileList.get(i)));
				}

			} else {
				for (File listX : fileList) {
					listX.delete();
				}
				String mString = null;
				if (malicious == true) {
					mString = "malicious activity";
				}
				UploadResponse ur = ctx.getBean(UploadResponse.class);
				if (dataX.equalsIgnoreCase("true")) {
					ur.setStatus(emptyLocation + ", " + ", " + mString);
				} else
					ur.setStatus(dataX + ", " + emptyLocation + ", " + ", " + malicious);

				responseList.add(ur);
			}
		} finally {
			api.disconnectCabinet(sessionId);
		}
		return responseList;
	}

	public List<UploadResponse> caseUpload(ArrayList<CaseData> cd, String sessionId) {

		CabinetAPI api = null;
		ArrayList<UploadResponse> responseList = null;
		try {
			responseList = (ArrayList) ctx.getBean("responseList");
			api = ctx.getBean(CabinetAPI.class);
			// Check 1 for Session
			String dataX = validateSessionId(sessionId);
			// Check 2 for emptyCaseId
			String caseManagment = "";
			for (CaseData cdx : cd) {
				if (cdx.getCaseId() == null || cdx.getCaseId() == "" || cdx.getLeadId() == null
						|| cdx.getLeadId() == "") {
					caseManagment = caseManagmentX;
					break;
				}
			}
			List<File> fileList = (ArrayList) ctx.getBean("fileList");
			boolean malicious = false;
			for (CaseData cdX : cd) {
				try {
					File tempFile = bc.writeByteArraysToFile(PropertiesReader.getProp().getProperty("fileLocation")
							+ cdX.getCaseId() + " " + cdX.getDocumentType() + " " + cdX.getDocument() + "."
							+ FilenameUtils.getExtension(cdX.getDocN()), em.decodeData(cdX.getBinData()));
					if (isMaliciousItem(tempFile)) {
						malicious = true;
						tempFile.delete();
						break;
					} else {
						fileList.add(tempFile);
					}

				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			if (dataX.equalsIgnoreCase("true") && !(caseManagment.equals(caseManagmentX)) && malicious == false) {
				UploadDocument ud = ctx.getBean(UploadDocument.class);
				for (int i = 0; i < cd.size(); i++) {
					responseList.add(ud.upload(cd.get(i), sessionId, fileList.get(i)));
				}

			} else {
				for (File listX : fileList) {
					listX.delete();
				}
				String mString = null;
				if (malicious == true) {
					mString = "malicious activity";
				}
				UploadResponse ur = ctx.getBean(UploadResponse.class);
				if (dataX.equalsIgnoreCase("true")) {
					ur.setStatus(caseManagment + ", " + mString);
				} else
					ur.setStatus(dataX + ", " + caseManagment + ", " + mString);

				responseList.add(ur);
			}
		} finally {
			api.disconnectCabinet(sessionId);
		}
		return responseList;
	}

	public VersionResponse versionService(String sessionId, VersionRequest vr, String documentId) {
		logger.info("Entered version service");
		VersionResponse vrr = ctx.getBean(VersionResponse.class);
		IPiramalDBOperations ops = ctx.getBean(IPiramalDBOperations.class);

		// Blank Comment Validation
		String errMsg = "";
		if (vr.getComment() == "" || vr.getComment() == null)
			errMsg = blankCommentsX;

		// DocumentID Type Validation
		try {
			vr.setDocId(Integer.parseInt(documentId));
		} catch (NumberFormatException e) {
			errMsg += invalidDocumentId;
		}

		// DocumentID Exists Validation
		String sessionFlag = validateSessionId(sessionId);
		// Document Exists Validation
		List<ISIndexData> list = ops.getIsIndex(Integer.parseInt(documentId));

		// Malicious Check
		boolean malicious = false;
		File tempFile = null;
		String name = String.valueOf(Math.random());
		try {
			tempFile = bc.writeByteArraysToFile(
					PropertiesReader.getProp().getProperty("fileLocation") + name + "." + vr.getExt(),
					em.decodeData(vr.getData()));
			if (isMaliciousItem(tempFile)) {
				malicious = true;
				tempFile.delete();
				System.out.println("Malicious Check From Version Creation ");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (errMsg.equals("") && sessionFlag.equals("true") && list.size() > 0 && malicious == false) {
			// vrr.setComment("dhappa");
			DocumentVersioning dv = ctx.getBean(DocumentVersioning.class);
			return dv.checkin(vr, sessionId, list.get(0), tempFile);
		} else {
			if (list.size() == 0) {
				errMsg += "no document found with this document id";
			}
			if (malicious == true) {
				errMsg += "malicious file detected removing file";
			}
			vrr.setComment(errMsg + ", " + sessionFlag);
		}
		return vrr;
	}

	public List<BinaryData> downloadDocuments(String sessionId, String... documents) {
		String sessionFlag = validateSessionId(sessionId);
		List<BinaryData> downloadList = (ArrayList) ctx.getBean("downloadList");
		if (sessionFlag.equals("true")) {
			DownloadDocument dd = ctx.getBean(DownloadDocument.class);
			for (String document : documents) {
				downloadList.add(dd.documentDownload(Integer.valueOf(document), sessionId));
			}
		} else {
			BinaryData bd = ctx.getBean(BinaryData.class);
			bd.setError(sessionFlag);
			downloadList.add(bd);
		}

		logger.info(downloadList);
		return downloadList;
	}

	public List<JSONObject> getDocumentMetaData(String sessionId, MetaDataRequest metaDataRequest) {
		IPiramalDBOperations ops = ctx.getBean(IPiramalDBOperations.class);

		if (("Invalid CallerSystem").equals(ops.validatecallerSystem(metaDataRequest.getCallerSystem()))) {
			throw new PiramalException("There is some mistake in caller system name");
		}

		String prop = null;
		if (metaDataRequest.getCallerSystem().equalsIgnoreCase("SALESFORCE")) {
			prop = PropertiesReader.getProp().getProperty("SalesforceWhiteList");
		} else if (metaDataRequest.getCallerSystem().equalsIgnoreCase("PENNANT")) {
			prop = PropertiesReader.getProp().getProperty("PennantWhiteList");
		} else if (metaDataRequest.getCallerSystem().equalsIgnoreCase("UNO")) {
			prop = PropertiesReader.getProp().getProperty("UNOWhiteList");
		} else if (metaDataRequest.getCallerSystem().equalsIgnoreCase("WEBSITE")) {
			prop = PropertiesReader.getProp().getProperty("websiteWhiteList");
		}

		String[] props = prop.split(",");
		for (int i = 0; i < props.length; i++) {
			System.out.print("props[" + i + "]= " + props[i] + "\t");
			logger.info("props[" + i + "]= " + props[i] + "\t");
		}
		logger.info("Caller system properties are as like: " + props);
		List<Integer> docIdList = (List) ctx.getBean("docIdList");
		List<JSONObject> jsonObject = (List) ctx.getBean("jsonObject");

		CabinetAPI api = ctx.getBean(CabinetAPI.class);
		// for(String id:leadId) {
		// docIdList.addAll(ops.getDocIdFromLeadId(id));
		// }
		// Putting map for 3rd filter of value updatedFlag
		Map<String, String> fieldMap = new HashMap<>();
		if (!metaDataRequest.getUpdatedFlag().isEmpty()) {
			if (metaDataRequest.getCallerSystem().equalsIgnoreCase("SALESFORCE")) {
				fieldMap.put(PropertiesReader.getProp().getProperty("salesforceField"),
						metaDataRequest.getUpdatedFlag());
			} else if (metaDataRequest.getCallerSystem().equalsIgnoreCase("PENNANT")) {
				fieldMap.put(PropertiesReader.getProp().getProperty("pennantField"), metaDataRequest.getUpdatedFlag());
			} else if (metaDataRequest.getCallerSystem().equalsIgnoreCase("UNO")) {
				fieldMap.put(PropertiesReader.getProp().getProperty("unoField"), metaDataRequest.getUpdatedFlag());
			}
		}
		logger.info("Getting doc ids with filter & reading fields from properties file...");
		// 1st & 3rd Filter is applied with the query
		docIdList.addAll(ops.getDocIdFromLeadIdFilterByDoctypes(metaDataRequest.getLeadId(), props, fieldMap));
		logger.info("docIdList= " + docIdList);
		DMSXmlResponse xmlResponse;
		JSONObject jObj;
		Timestamp revisedDate;
		// yyyy-mm-dd hh:mm:ss[.fffffffff]updatedAfterFilterRequired
		boolean updatedAfterFilterRequired = false;
		Timestamp updatedAfter = null;
		logger.info("updatedAfter= " + metaDataRequest.getUpdatedAfter());
		if (!(metaDataRequest.getUpdatedAfter().trim().isEmpty())) {
			updatedAfter = Timestamp.valueOf(metaDataRequest.getUpdatedAfter());
			updatedAfterFilterRequired = true;
		}
		logger.info("updated after=" + updatedAfter);
		for (Integer i : docIdList) {
			xmlResponse = api.getDocumentProperty(sessionId, i);
			logger.info("xmlResponse= " + xmlResponse);
			logger.info("Line 1 checked..............");
			System.out.println("Line 1 checked sys..............");
			// 2nd Filter check RevisedDateTime is after updateAfter input value
			if (updatedAfterFilterRequired == true) {
				String rDate = getPropertyValuefromXml(xmlResponse.toString(), "RevisedDateTime");
				logger.info("Line 1 checked..............");
				System.out.println("Line 2 checked sys..............");
				logger.info("RevisedDateTime from xmlResponse" + rDate);
				revisedDate = Timestamp.valueOf(rDate);
				logger.info("revisedDate= " + revisedDate);
				logger.info("updatedAfter= " + updatedAfter);
				if (!revisedDate.after(updatedAfter)) {
					logger.info("revisedDate  is not After updatedAfter");
					continue;
				} else {
					logger.info("revisedDate  is After updatedAfter");
					// logger.info(xmlResponse);
				}
			}
			jObj = XML.toJSONObject(xmlResponse.toString());
			logger.info("Object to add is" + jObj);
			boolean isAdded = jsonObject.add(jObj);
			logger.info("isAdded= " + isAdded);

		}
		api.disconnectCabinet(sessionId);
		logger.info("Returning jsonObject= " + jsonObject);
		return jsonObject;
	}

	public List<UploadResponse> updateDocumentMetaData(String sessionId,
			ArrayList<UpdateMetaDataRequest> updateMetaDataRequest) {
		String dataX = validateSessionId(sessionId);
		ArrayList<UploadResponse> responseList = (ArrayList) ctx.getBean("responseList");
		if (dataX.equalsIgnoreCase("true")) {
			UpdateMetaData umd = ctx.getBean(UpdateMetaData.class);
			CabinetAPI api = ctx.getBean(CabinetAPI.class);
			for (UpdateMetaDataRequest umdr : updateMetaDataRequest) {
				responseList.add(umd.updateMetaData(sessionId, umdr));
				logger.info("responseList= " + responseList);
			}
			api.disconnectCabinet(sessionId);

		} else {
			UploadResponse ur = ctx.getBean(UploadResponse.class);
			ur.setStatus("Invalid Session");
			responseList.add(ur);
			return responseList;
		}

		return responseList;
	}

	String getPropertyValuefromXml(String xmlString, String property) {
		String pValue = "";
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xmlString));
			Document doc = builder.parse(is);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList flowList = doc.getElementsByTagName("Document");
			for (int i = 0; i < flowList.getLength(); i++) {
				NodeList childList = flowList.item(i).getChildNodes();
				for (int j = 0; j < childList.getLength(); j++) {
					Node childNode = childList.item(j);
					if (property.equals(childNode.getNodeName())) {
						System.out.println(childList.item(j).getTextContent().trim());
						pValue = childList.item(j).getTextContent().trim();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pValue;

	}

}
