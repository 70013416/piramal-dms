/**
 * @author Pratik
 *
 */
package com.awc.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Pratik
 *
 */
public class PropertiesReader {

	/**
	 * @param args
	 */
	public static Properties readPropFile() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			
			input = new FileInputStream("db.properties");
		    prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
}

