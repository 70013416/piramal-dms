/**
 * 
 */
package com.awc.uno.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.awc.dto.DocumentDetails;
import com.awc.dto.InputJSON;
import com.awc.dto.OutputJson;
import com.awc.services.dmsapi.CabinetApi;

/**
 * @author Pratik
 *
 */
public class UnoDocumentDetails {
	final static Logger logger = Logger.getLogger(UnoDocumentDetails.class);
	private URL url;
	private HttpURLConnection conn;
	private OutputJson json;
	private UnoGetToken ugt;
	private UnoEncryptText uet;
	private OutputJson oj;
	private String outputx;

	public UnoDocumentDetails() {
		json = new OutputJson();
		ugt = new UnoGetToken();
		uet = new UnoEncryptText();
		oj = new OutputJson();
	}

	public String pushDataToUno(DocumentDetails dd) {
		outputx=null;
		try {
			url = new URL("https://uatphfl.unoonline.me//UATPHFLDMS/OD.svc/DocumentDetails");
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			String tokenx = null;
			tokenx = ugt.getToken();
			tokenx = tokenx.substring(27, tokenx.lastIndexOf("'"));
			// System.out.println("TokenX :"+tokenx);
			String encryptedData = uet.encryptData(dd, tokenx);

			// System.out.println("Encrypted Data : "+encryptedData);
			// System.out.println("Trimmed Data : "+encryptedData.substring(1,
			// encryptedData.length()-1));

			oj.setToken(tokenx);
			oj.setEFormat(encryptedData.substring(1, encryptedData.length() - 1));

			String input = oj.toString();
			logger.info("Input JSON from UnoDocumentDetails API : " + input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output=null;
			logger.info("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				logger.info(output);
				outputx=output;
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return outputx;
	}

	/*public static void main(String[] args) {
		UnoDocumentDetails udd = new UnoDocumentDetails();
		DocumentDetails dd = new DocumentDetails();
		dd.setDid("A124");
		dd.setDIdx("310");
		dd.setLid("2019041800012");
		dd.setDTyp("PROPERTY");
		dd.setDoc("fdghfgh");
		dd.setApp("Applicant");
		dd.setUNm("NEEL");
		// dd.setDNm("GI insurance Policy");
		dd.setDext("jpeg");

		for (int i = 8000; i < 8200; i++) {
			dd.setDIdx("300" + i);
			udd.pushDataToUno(dd);
		}

	}*/
}
