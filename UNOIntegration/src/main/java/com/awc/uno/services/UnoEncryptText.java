/**
 * 
 */
package com.awc.uno.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.awc.dto.DocumentDetails;
import com.awc.dto.InputJSON;

/**
 * @author Pratik
 *
 */
public class UnoEncryptText {
	final static Logger logger = Logger.getLogger(UnoEncryptText.class);
	private String outputx;
	//private String tokenx;
	private InputJSON json;
	
	private URL url;
	private HttpURLConnection conn;
	public UnoEncryptText() {
		json = new InputJSON();
	}
	public String encryptData(DocumentDetails dd,String myToken) {

		try {
			url = new URL("https://uatphfl.unoonline.me//UATPHFLDMS/OD.svc/Encrypttextobject");
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			
			

			//System.out.println("Test Token : " + tokenx);
			json.setEFormat(dd);
			//json.setEFormat("Pratik");
			json.setToken(myToken);
			String input = json.toString();

			logger.info("Inout Json : " + input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			logger.info("Output from Server On Data Encryption....  \n");
			while ((output = br.readLine()) != null) {
				logger.info("Encrypted Data : "+output);
				outputx = output;
			}

			conn.disconnect();
			
		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return outputx;
	}
	/*public static void main(String[] args) {
		UnoEncryptText uet=new UnoEncryptText();
		UnoGetToken ugt=new UnoGetToken();
		String tokenx=null;
		tokenx = ugt.getToken().substring(27, 51);
		uet.encryptData(new DocumentDetails("A124", "31", "2019060100001", "PROPERTY", "fdghfgh", "Applicant",
				"NEEL", "GI insurance Policy", "jpeg"),tokenx);
	}*/
}
