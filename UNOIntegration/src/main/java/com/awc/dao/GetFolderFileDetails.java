/**
 * 
 */
package com.awc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.dbconnection.ConnectPiramalDB;
import com.awc.dto.DocumentDetails;
import com.awc.properties.PropertiesReader;
import com.awc.services.dmsapi.CabinetApi;
import com.awc.uno.services.UnoDocumentDetails;
import com.newgen.dmsapi.DMSXmlResponse;

/**
 * @author Pratik
 *
 */
public class GetFolderFileDetails {
	private ConnectPiramalDB db;
	private Connection connection;
	private DMSXmlResponse xmlresponse;
	private CabinetApi api;
	private String sessionId=null;
	final static Logger logger = Logger.getLogger(GetFolderFileDetails.class);
	//Queries Start
	
	private String dataSetQuery="select doc.DocumentIndex,doc.ImageIndex,dc.Field_20,dc.Field_21,dc.Field_22,dc.Field_23,dc.FIELD_24,doc.Name,doc.AppName from PDBDocument doc,DDT_5 dc\r\n" + 
			"where (dc.FIELD_40 <>'Y') and dc.FoldDocIndex in(select DocumentIndex from PDBDocumentContent where ParentFolderIndex\r\n" + 
			"in (select FolderIndex from PDBFolder where ParentFolderIndex in(select FolderIndex from PDBFolder where name like '%Checker-Approve%' and ParentFolderIndex \r\n" + 
			"in (select FolderIndex from PDBFolder where ParentFolderIndex=?)))) and dc.FoldDocIndex=doc.DocumentIndex";
	
	
	
	//Queries Closed
	
	private PreparedStatement dataSetPs;
	private ResultSet dataSetRs;
	private static Properties pp;
	private DocumentDetails dd;
	public GetFolderFileDetails(){
		db = new ConnectPiramalDB();
		dd = new DocumentDetails();
		connection=db.getConnection();
		pp=PropertiesReader.readPropFile();
		
		api=new CabinetApi();
		xmlresponse=new DMSXmlResponse();
		xmlresponse.setXmlString(api.connectCabinet());
		if (xmlresponse.getVal("Status").equalsIgnoreCase("0")) {
			sessionId = xmlresponse.getVal("UserDBId");
			logger.info("SessionId obtained :"+sessionId);
			//logger.info("SessionId obtained :"+sessionId);
		}
		else {
			logger.error("Error Getting SessionId : "+xmlresponse.getVal("Error"));
			System.err.println("Error Getting SessionId : "+xmlresponse.getVal("Error"));
		}
	}
	
	/**
	 * @return the api
	 */
	public CabinetApi getApi() {
		return api;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	public void getDataFromDms() {
		try {
			dataSetPs=connection.prepareCall(dataSetQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//System.out.println("Data -> "+pp.getProperty("rootFolderIndex"));
			dataSetPs.setInt(1, Integer.valueOf(pp.getProperty("rootFolderIndex")));
			dataSetRs=dataSetPs.executeQuery();
			
			while(dataSetRs.next()) {
				UnoDocumentDetails udd=null;
				dd.setDid(String.valueOf(dataSetRs.getInt(1)));
				dd.setDIdx(String.valueOf(dataSetRs.getInt(2)));
				dd.setLid(dataSetRs.getString(3));
				dd.setDTyp(dataSetRs.getString(4));
				dd.setDoc(dataSetRs.getString(5));
				dd.setApp(dataSetRs.getString(6));
				dd.setUNm(dataSetRs.getString(7));
				dd.setDNm(dataSetRs.getString(8));
				dd.setDext(dataSetRs.getString(9));
				String response=null;
				udd=new UnoDocumentDetails();
				response=udd.pushDataToUno(dd);
				logger.info("UNO Integration -> "+response);
				logger.info("Document Details From UnoIntegration :"+dd);
				if(response.contains("SUCCESS")) {
					logger.info("Logger After Meta-Data Update : "+updateMetaData(String.valueOf(dataSetRs.getInt(1)), dataSetRs.getString(8),"Y"));
				}
				else {
					logger.info("Document Details  Failed :"+dd+"\n With Error : "+updateMetaData(String.valueOf(dataSetRs.getInt(1)), dataSetRs.getString(8),response));
				}
					
				 
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	public String updateMetaData(String docId,String docName,String response) {
		
		String inputXml="<?xml version=\"1.0\"?>\r\n" + 
				"<NGOChangeDocumentProperty_Input>\r\n" + 
				"    <Option>NGOChangeDocumentProperty</Option>\r\n" + 
				"    <CabinetName>piramal</CabinetName>\r\n" + 
				"    <UserDBId>"+ sessionId +"</UserDBId>\r\n" + 
				"    <GroupIndex>0</GroupIndex>\r\n" + 
				"    <Document>\r\n" + 
				"        <DocumentIndex>"+docId+"</DocumentIndex>\r\n" + 
				"        <DocumentName>"+docName+"</DocumentName>\r\n" + 
				"        <Owner>Supervisor</Owner>\r\n" + 
				"        <VersionFlag></VersionFlag>\r\n" + 
				"        <Comment></Comment>\r\n" + 
				"        <DataDefinition>\r\n" + 
				"            <DataDefName>document</DataDefName>\r\n" + 
				"            <Fields>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>40</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>"+response+"</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"            </Fields>\r\n" + 
				"        </DataDefinition>\r\n" + 
				"        <OwnerIndex></OwnerIndex>\r\n" + 
				"        <OwnerType>U</OwnerType>\r\n" + 
				"    </Document>\r\n" + 
				"</NGOChangeDocumentProperty_Input>";
		
		return api.callBroker(inputXml);
	}
	
	public static void main(String[] args) {
		GetFolderFileDetails gfd=new GetFolderFileDetails();
		gfd.getDataFromDms();
		gfd.getApi().disconnectCabinet(gfd.getSessionId());
	}
}
