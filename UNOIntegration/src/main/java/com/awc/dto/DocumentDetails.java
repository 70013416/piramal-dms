/**
 * 
 */
package com.awc.dto;

/**
 * @author Pratik
 *
 */
public class DocumentDetails {
	String Did,DIdx,Lid,DTyp,Doc,App,UNm,DNm,Dext;
	
	public DocumentDetails() {
	}

	/**
	 * @param did
	 * @param dIdx
	 * @param lid
	 * @param dTyp
	 * @param doc
	 * @param app
	 * @param uNm
	 * @param dNm
	 * @param dext
	 */
	public DocumentDetails(String did, String dIdx, String lid, String dTyp, String doc, String app, String uNm,
			String dNm, String dext) {
		super();
		Did = did;
		DIdx = dIdx;
		Lid = lid;
		DTyp = dTyp;
		Doc = doc;
		App = app;
		UNm = uNm;
		DNm = dNm;
		Dext = dext;
	}

	public String getDid() {
		return Did;
	}

	public void setDid(String did) {
		Did = did;
	}

	public String getDIdx() {
		return DIdx;
	}

	public void setDIdx(String dIdx) {
		DIdx = dIdx;
	}

	public String getLid() {
		return Lid;
	}

	public void setLid(String lid) {
		Lid = lid;
	}

	public String getDTyp() {
		return DTyp;
	}

	public void setDTyp(String dTyp) {
		DTyp = dTyp;
	}

	public String getDoc() {
		return Doc;
	}

	public void setDoc(String doc) {
		Doc = doc;
	}

	public String getApp() {
		return App;
	}

	public void setApp(String app) {
		App = app;
	}

	public String getUNm() {
		return UNm;
	}

	public void setUNm(String uNm) {
		UNm = uNm;
	}

	public String getDNm() {
		return DNm;
	}

	public void setDNm(String dNm) {
		DNm = dNm;
	}

	public String getDext() {
		return Dext;
	}

	public void setDext(String dext) {
		Dext = dext;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((App == null) ? 0 : App.hashCode());
		result = prime * result + ((DIdx == null) ? 0 : DIdx.hashCode());
		result = prime * result + ((DNm == null) ? 0 : DNm.hashCode());
		result = prime * result + ((DTyp == null) ? 0 : DTyp.hashCode());
		result = prime * result + ((Dext == null) ? 0 : Dext.hashCode());
		result = prime * result + ((Did == null) ? 0 : Did.hashCode());
		result = prime * result + ((Doc == null) ? 0 : Doc.hashCode());
		result = prime * result + ((Lid == null) ? 0 : Lid.hashCode());
		result = prime * result + ((UNm == null) ? 0 : UNm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentDetails other = (DocumentDetails) obj;
		if (App == null) {
			if (other.App != null)
				return false;
		} else if (!App.equals(other.App))
			return false;
		if (DIdx == null) {
			if (other.DIdx != null)
				return false;
		} else if (!DIdx.equals(other.DIdx))
			return false;
		if (DNm == null) {
			if (other.DNm != null)
				return false;
		} else if (!DNm.equals(other.DNm))
			return false;
		if (DTyp == null) {
			if (other.DTyp != null)
				return false;
		} else if (!DTyp.equals(other.DTyp))
			return false;
		if (Dext == null) {
			if (other.Dext != null)
				return false;
		} else if (!Dext.equals(other.Dext))
			return false;
		if (Did == null) {
			if (other.Did != null)
				return false;
		} else if (!Did.equals(other.Did))
			return false;
		if (Doc == null) {
			if (other.Doc != null)
				return false;
		} else if (!Doc.equals(other.Doc))
			return false;
		if (Lid == null) {
			if (other.Lid != null)
				return false;
		} else if (!Lid.equals(other.Lid))
			return false;
		if (UNm == null) {
			if (other.UNm != null)
				return false;
		} else if (!UNm.equals(other.UNm))
			return false;
		return true;
	}

	/*@Override
	public String toString() {
		return "DocumentDetails [Did=" + Did + ", DIdx=" + DIdx + ", Lid=" + Lid + ", DTyp=" + DTyp + ", Doc=" + Doc
				+ ", App=" + App + ", UNm=" + UNm + ", DNm=" + DNm + ", Dext=" + Dext + "]";
	}*/
	/*@Override
	public String toString() {
		return "{\'Details\': "+"[\'[{\'DId\':\'"+Did+"\',\'DIdx\':\'"+DIdx+"\',\'LId\':\'"+Lid+"\',"
				+ "\'DTyp\':\'"+DTyp+"\',\'Doc\':\'"+Doc+"\',\'App\':\'"+App+"\'"
				+ ",\'UNm\':\'"+UNm+"\',\'DNm\':\'"+DNm+"\',\'Dext\':\'."+Dext+"\'}]\']}";
	}*/
	/*@Override
	public String toString() {
		return "{\"Details\": [\"[{'DId':'"+Did+"','DIdx':'"+DIdx+"','LId':'"+Lid+"','DTyp':'"+DTyp+"','Doc':'"+Doc+"','App':'"+App+"','UNm':'"+UNm+"','DNm':'"+DNm+"','Dext':'."+Dext+"'}]\"]}";
	}*/
	@Override
	public String toString() {
		return "{\\\"Details\\\": [\\\"[{'DId':'"+Did+"','DIdx':'"+DIdx+"','LId':'"+Lid+"','DTyp':'"+DTyp+"','Doc':'"+Doc+"','App':'"+App+"','UNm':'"+UNm+"','DNm':'"+DNm+"','Dext':'."+Dext+"'}]\\\"]}";
	}
	/*@Override
	public String toString() {
		return "[{\'DId\':\'"+Did+"\',\'DIdx\':\'"+DIdx+"\',\'LId\':\'"+Lid+"\',"
				+ "\'DTyp\':\'"+DTyp+"\',\'Doc\':\'"+Doc+"\',\'App\':\'"+App+"\'"
				+ ",\'UNm\':\'"+UNm+"\',\'DNm\':\'"+DNm+"\',\'Dext\':\'."+Dext+"\'}]";
	}*/
	/*@Override
	public String toString() {
		return "[{\"DId\":\""+Did+"\",\"DIdx\":\""+DIdx+"\",\"LId\":\""+Lid+"\","
				+ "\"DTyp\":\""+DTyp+"\",\"Doc\":\""+Doc+"\",\"App\":\""+App+"\""
				+ ",\"UNm\":\""+UNm+"\",\"DNm\":\""+DNm+"\",\"Dext\":\"."+Dext+"\"}]";
	}*/
	public static void main(String[] args) {
		DocumentDetails dd=new DocumentDetails();
		dd.setDid("A124");
		dd.setDIdx("31");
		dd.setLid("2019060100001");
		dd.setDTyp("PROPERTY");
		dd.setDoc("fdghfgh");
		dd.setApp("Applicant");
		dd.setUNm("NEEL");
		dd.setDNm("GI insurance Policy");
		dd.setDext("jpeg");
		System.out.println(dd);
	}
}
