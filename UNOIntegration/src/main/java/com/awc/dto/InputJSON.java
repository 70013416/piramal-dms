/**
 * 
 */
package com.awc.dto;

/**
 * @author Pratik
 *
 */
public class InputJSON {
	private DocumentDetails EFormat;
	private String Token;
	
	public InputJSON() {
	}

	public InputJSON(DocumentDetails eFormat, String token) {
		super();
		EFormat = eFormat;
		Token = token;
	}

	public DocumentDetails getEFormat() {
		return EFormat;
	}

	public void setEFormat(DocumentDetails eFormat) {
		EFormat = eFormat;
	}

	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((EFormat == null) ? 0 : EFormat.hashCode());
		result = prime * result + ((Token == null) ? 0 : Token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InputJSON other = (InputJSON) obj;
		if (EFormat == null) {
			if (other.EFormat != null)
				return false;
		} else if (!EFormat.equals(other.EFormat))
			return false;
		if (Token == null) {
			if (other.Token != null)
				return false;
		} else if (!Token.equals(other.Token))
			return false;
		return true;
	}

	@Override
	public String toString() {
		//return "{\"EFormat\":\""+EFormat+"\",\"Token\": \""+Token+"\"}";
		return "{\"Token\": \""+Token+"\",\"EFormat\": \""+EFormat+"\"}";
		}
	
}
