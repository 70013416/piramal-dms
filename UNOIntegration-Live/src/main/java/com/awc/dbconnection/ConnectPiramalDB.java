/**
 * 
 */
package com.awc.dbconnection;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class ConnectPiramalDB {
	private BasicDataSource bds;
	private Properties prop;
	private Connection conn;

	/**
	 * 
	 */
	public ConnectPiramalDB() {
		bds = new BasicDataSource();
		prop = PropertiesReader.readPropFile();
	}

	public Connection getConnection() {

		try {
			//System.out.println(prop.getProperty("driver"));
			bds.setDriver((Driver) Class.forName(prop.getProperty("driver")).newInstance());
			bds.setUrl(prop.getProperty("url"));
			bds.setUsername(prop.getProperty("user"));
			bds.setPassword(prop.getProperty("password"));
			conn = bds.getConnection();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void main(String[] args) {
		ConnectPiramalDB db = new ConnectPiramalDB();
		System.out.println("Get Connection : "+db.getConnection());
	}
}
