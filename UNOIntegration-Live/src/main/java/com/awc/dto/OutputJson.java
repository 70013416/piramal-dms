/**
 * 
 */
package com.awc.dto;

/**
 * @author Pratik
 *
 */
public class OutputJson {
	private String Token,EFormat;
	public OutputJson() {
	}
	public OutputJson(String token, String eFormat) {
		super();
		Token = token;
		EFormat = eFormat;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getEFormat() {
		return EFormat;
	}
	public void setEFormat(String eFormat) {
		EFormat = eFormat;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((EFormat == null) ? 0 : EFormat.hashCode());
		result = prime * result + ((Token == null) ? 0 : Token.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OutputJson other = (OutputJson) obj;
		if (EFormat == null) {
			if (other.EFormat != null)
				return false;
		} else if (!EFormat.equals(other.EFormat))
			return false;
		if (Token == null) {
			if (other.Token != null)
				return false;
		} else if (!Token.equals(other.Token))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "{\"Token\": \""+Token+"\",\"EFormat\": \""+EFormat+"\"}";
	}
	
}
