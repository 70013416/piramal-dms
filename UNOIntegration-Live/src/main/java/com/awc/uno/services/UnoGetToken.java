package com.awc.uno.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UnoGetToken {
	private String outputx,output;

	public String getToken() {

		try {

			URL url = new URL("https://phfl.unoonline.me//PhflDMS/OD.svc/Gettoken");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			//conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			//System.out.println("Output from Server On Get Token....  \n");
			while ((output = br.readLine()) != null) {
				//System.out.println("Token JSON : " + output);
				outputx=output;
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return outputx;
	}

	public static void main(String[] args) {
		UnoGetToken ugt = new UnoGetToken();
		for(int i=0;i<25;i++) {
			//System.out.println(ugt.getToken());
			String demoText=ugt.getToken();
			System.out.println(demoText);
			demoText=demoText.substring(27,demoText.lastIndexOf("'"));
			System.out.println(demoText);
		}
	}
}
