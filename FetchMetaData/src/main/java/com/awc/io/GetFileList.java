/**
 * 
 */
package com.awc.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class GetFileList {
	static private Properties pp;
	final static Logger logger = Logger.getLogger(GetFileList.class);
	private CsvWriter csvWriter;
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S");
	static {
		pp = PropertiesReader.readPropFile();
	}
	public GetFileList() {
		csvWriter=new CsvWriter();
	}
	private static String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}
	public void getFileList(String pathName) throws IOException {
		/*
		 * for(BarCodes bc:BarCodes.values()) { System.out.println(bc.getLabel()); }
		 */
		File f=new File(pathName);
		File fileList[]=f.listFiles();
		for(File tempFile:fileList) {
			if(tempFile.isDirectory()) {
				getFileList(tempFile.getAbsolutePath());
			}
			else {
				if(getFileExtension(tempFile).equalsIgnoreCase("pdf")) {
					Path p=Paths.get(tempFile.getAbsolutePath());
					BasicFileAttributes view = Files.getFileAttributeView(p, BasicFileAttributeView.class).readAttributes();
	        		FileTime fileTime=view.creationTime();
	        		csvWriter.writeCsv(tempFile, sdf.format(fileTime.toMillis()), tempFile.getName().split("\\$"));
	        	}
			}
		}
		//csvWriter.getCsvWriter().close();
	}
	public static void main(String[] args) {
		GetFileList gfl=new GetFileList();
		try {
			gfl.getFileList(pp.getProperty("pathName"));
		}
		catch(IOException e) {
			e.printStackTrace();
			logger.error(e);
			
		}
	}
}
