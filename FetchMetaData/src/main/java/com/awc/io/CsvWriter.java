/**
 * 
 */
package com.awc.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class CsvWriter {
	private Properties pp;
	final static Logger logger = Logger.getLogger(CsvWriter.class);
	private FileWriter csvWriter = null;
	private String dataLAF="OTHERS,Loan Application Form,";

	public CsvWriter() {
		try {
			pp = PropertiesReader.readPropFile();
			csvWriter = new FileWriter(pp.getProperty("csvFile"));
			csvWriter.append("FilePath,");
			csvWriter.append("FileCreationDate,");
			csvWriter.append("LeadId,");
			csvWriter.append("DocumentType,");
			csvWriter.append("DocumentName,");
			csvWriter.append("Applicant,");
			csvWriter.append("Size,");
			csvWriter.append("\n");
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public FileWriter getCsvWriter() {
		return csvWriter;
	}

	public void setCsvWriter(FileWriter csvWriter) {
		this.csvWriter = csvWriter;
	}

	public void writeCsv(File file, String date, String[] arr) {
		try {
			if(arr.length==4 || arr.length==3) {
				if (arr.length == 4) {
					csvWriter.append(file.getAbsolutePath() + ",");
					csvWriter.append(date + ",");
					for (int i = 0; i < arr.length; i++) {
						if (i == 3) {
							csvWriter.append(arr[i].substring(0, arr[i].lastIndexOf("."))+","+file.length());
						} else {
							csvWriter.append(arr[i] + ",");
						}
					}
					csvWriter.append("\n");
					csvWriter.flush();
				} else if (arr.length == 3) {
					String data="";
					for (int i = 0; i < arr.length; i++) {
						if (i == 2) {
							data+=(arr[i].substring(0, arr[i].lastIndexOf(".")));
						} else if (i == 1 && arr[i].equals("Loan Application Form") || i == 1 && arr[i].equals("Loan Application Form_1") || i == 1 && arr[i].equals("Loan Application Form_2") || i == 1 && arr[i].equals("Loan Application Form_3") ||i == 1 && arr[i].equals("Loan Application Form_4") || i == 1 && arr[i].equals("Loan Application Form_5")) {
							data+=(dataLAF);
						} else {
							data+=(arr[i] + ",");
						}
					}
					if(data.contains(dataLAF)) {
						csvWriter.append(file.getAbsolutePath() + ",");
						csvWriter.append(date + ",");
						csvWriter.append(data+","+file.length());
						csvWriter.append("\n");
						csvWriter.flush();
					}
					else {
						logger.error("Issue in File :" + file.getAbsolutePath());
					}
				}
			}
			 else {
				logger.error("Issue in File :" + file.getAbsolutePath());
			}

		} catch (IOException e) {
			logger.error(e);
		}

	}
}
