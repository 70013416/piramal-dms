/**
 * 
 */
package com.awc.enums;

/**
 * @author Pratik
 *
 */
public enum DocumentType {
	INCOME("INCOME"),KYC("KYC"),PROPERTY("PROPERTY"),OTHERS("OTHERS");

	private final String label;
	private DocumentType(String label) {
		this.label=label;
	}
	public String getLabel() {
		return this.label;
	}
}
