/**
 * 
 */
package com.awc.enums;

/**
 * @author Pratik
 *
 */
public enum Applicant {
	Applicant("Applicant"),coapplicant1("coapplicant1"),Coapplicant2("Coapplicant2"),
	Coapplicant3("Coapplicant3"),Coapplicant4("Coapplicant4"),Coapplicant5("Coapplicant5"),
	Coapplicant6("Coapplicant6");
	private String label;
	private Applicant(String label) {
		this.label=label;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	
}
