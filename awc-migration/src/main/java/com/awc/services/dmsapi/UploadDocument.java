/**
 * 
 */
package com.awc.services.dmsapi;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormatSymbols;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.dto.DocumentData;
import com.awc.dto.UploadResponse;
import com.awc.properties.PropertiesReader;
import com.itextpdf.text.pdf.PdfReader;
import com.newgen.dmsapi.DMSXmlResponse;

import ISPack.CPISDocumentTxn;
import ISPack.ISUtil.JPDBRecoverDocData;
import ISPack.ISUtil.JPISException;
import ISPack.ISUtil.JPISIsIndex;

/**
 * @author Pratik
 *
 */
public class UploadDocument {
	final static Logger logger = Logger.getLogger(UploadDocument.class);
	
	private JPDBRecoverDocData docDBData;
	private JPISIsIndex newIsIndex;
	private DMSXmlResponse resp;
	private int docIndex;
	private String isIndex = null;
	private CabinetApi api;
	private Properties pp;
	private PdfReader pdfReader;
	

	public UploadDocument() {
		docDBData = new JPDBRecoverDocData();
		newIsIndex = new JPISIsIndex();
		api = new CabinetApi();
		resp = new DMSXmlResponse();
		pp = PropertiesReader.readPropFile();
	}

	private static String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	public String getDocType(String ext) {
		String docType = "";
		if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("JPEG") || ext.equalsIgnoreCase("png")
				|| ext.equalsIgnoreCase("tiff") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("png")) {
			logger.info("Doctype is Image");
			docType = "I";
		} else if (ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("vob") || ext.equalsIgnoreCase("mkv")
				|| ext.equalsIgnoreCase("webm") || ext.equalsIgnoreCase("avi")) {
			logger.info("Doctype is Video");
			docType = "V";
		} else if (ext.equalsIgnoreCase("pdf")) {
			docType = "I";
		} else {
			logger.info("Doctype is Others");
			docType = "N";
		}
		return docType;
	}

	public UploadResponse upload(DocumentData dd,String sessionId) {
			UploadResponse ur=null;
			ur = new UploadResponse();
			File tempFile=null;
			PiramalDBOperations dbOps = null;
			dbOps = new PiramalDBOperations();
			
			try {
				
				int folderIndex = dbOps.validateCreateFolder(dd,sessionId);
				dd.setDocN(dd.getLeadId()+" "+dd.getDocumentType()+" "+dd.getDocument()+" "+dd.getApplicant()+dd.getFilePath().substring(dd.getFilePath().lastIndexOf(".")));
				tempFile=new File(dd.getFilePath());
				String docType = getDocType(getFileExtension(tempFile));
				String fileName=null;
				fileName=dd.getDocN().substring(0, dd.getDocN().length()-(getFileExtension(tempFile).length()+1));
				int pageNum=1;
				if(getFileExtension(tempFile).equals("pdf")) {
					pdfReader=new PdfReader(tempFile.toString());
					pageNum=pdfReader.getNumberOfPages();
					pdfReader.close();
					pdfReader=null;
				}
				
				
				String xmlData = "<?xml version=\"1.0\"?><NGOAddDocument_Input><Option>NGOAddDocument</Option><CabinetName>"
				+ pp.getProperty("cabinetName") + "</CabinetName><UserDBId>" + sessionId
				+ "</UserDBId><GroupIndex></GroupIndex><Document><ParentFolderIndex>" + folderIndex
				+ "</ParentFolderIndex><NoOfPages>"+pageNum+"</NoOfPages><AccessType>S</AccessType><DocumentName>"
				+ fileName
				+ "</DocumentName><CreationDateTime>"+dd.getFileCreationDate()+"</CreationDateTime><ExpiryDateTime></ExpiryDateTime><VersionFlag>N</VersionFlag><DocumentType>"
				+ docType + "</DocumentType><DocumentSize>" + tempFile.length()
				+ "</DocumentSize><CreatedByApp></CreatedByApp><CreatedByAppName>" + getFileExtension(tempFile)
				+ "</CreatedByAppName><ISIndex></ISIndex><FTSDocumentIndex>0</FTSDocumentIndex><ODMADocumentIndex></ODMADocumentIndex><Comment></Comment><Author></Author><OwnerIndex>1</OwnerIndex><EnableLog>Y</EnableLog><FTSFlag>PP</FTSFlag>"
				+ "<DataDefinition>\r\n" + 
				"<DataDefIndex>3</DataDefIndex><DataDefName>document</DataDefName><Fields>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>9</IndexId><IndexName>leadId</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+dd.getLeadId()+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>10</IndexId><IndexName>documentType</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+dd.getDocumentType()+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>11</IndexId><IndexName>document</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+dd.getDocument()+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>12</IndexId><IndexName>applicant</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+dd.getApplicant()+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>13</IndexId><IndexName>userUploaded</IndexName><IndexType>S</IndexType><IndexLength>255</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+dd.getUserName()+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>14</IndexId><IndexName>original</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>N</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>15</IndexId><IndexName>UNOIntegration</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>Y</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>16</IndexId><IndexName>MetaDataUpdate</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>Y</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"<Field>\r\n" + 
				"<IndexId>17</IndexId><IndexName>Branch</IndexName><IndexType>S</IndexType><IndexLength>50</IndexLength><IndexAttribute>4</IndexAttribute><IndexValue>"+pp.getProperty("branch")+"</IndexValue><Pickable>N</Pickable></Field>\r\n" + 
				"</Fields>\r\n" + 
				"</DataDefinition>"
				+ "<NameLength>255</NameLength></Document></NGOAddDocument_Input>";
				String outputXml = null;
				CPISDocumentTxn.AddDocument_MT(null, pp.getProperty("serverIp"),
						Short.parseShort(pp.getProperty("jtsPort")), pp.getProperty("cabinetName"),
						Short.parseShort("1"), dd.getFilePath(), docDBData, "1", newIsIndex);
				docIndex = newIsIndex.m_nDocIndex;
				isIndex = newIsIndex.m_nDocIndex + "#" + newIsIndex.m_sVolumeId;
				logger.info("DocIndex :" + docIndex + " /isIndex :" + isIndex);
				String frontData = xmlData.substring(0, xmlData.indexOf("<ISIndex>") + 9);
				String endData = xmlData.substring(xmlData.indexOf("</ISIndex>"), xmlData.length());
				xmlData = frontData + isIndex + endData;
				
				//logger.info("Input XML :" + xmlData);

				outputXml = api.callBroker(xmlData);
				resp.setXmlString(outputXml);

				//logger.info("Upload Call :"+resp);
				String indexParse = resp.getVal("ISIndex");
				indexParse = indexParse.substring(0, indexParse.indexOf("#"));
				ur.setDocId(Integer.parseInt(resp.getVal("DocumentIndex")));
				ur.setStatus(resp.getVal("Status"));
				ur.setImageIndex(Integer.parseInt(indexParse));
				logger.info("Call Broker to add document :" + outputXml);
				
				//logger.info("Disconnecting from Cabinet ::"+api.disconnectCabinet(sessionId));
				

			} catch (IOException | JPISException e) {
				e.printStackTrace();
			}
			finally {
				
					dbOps=null;
					logger.info("*****************************************");
					logger.info("File For Compression Deletion: "+tempFile.getAbsolutePath());
					tempFile=null;
					try {
						Files.delete(Paths.get(dd.getFilePath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
					logger.info("*****************************************");
				
			}
			return ur;
	}
	static String getMonthForInt(int num) {
		String month = "wrong";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (num >= 0 && num <= 11) {
			month = months[num];
		}
		logger.info("Month :" + month);
		return month;
	}
}