/**
 * 
 */
package com.awc.services.dmsapi;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.properties.PropertiesReader;
import com.newgen.dmsapi.DMSCallBroker;
import com.newgen.dmsapi.DMSInputXml;
import com.newgen.dmsapi.DMSXmlResponse;

/**
 * @author Pratik
 *
 */
public class CabinetApi {
	final static Logger logger = Logger.getLogger(CabinetApi.class);
	private DMSInputXml inputxml;
	private DMSXmlResponse xmlresponse;
	private static Properties pp;
	static {
		pp=PropertiesReader.readPropFile();
	}
	public CabinetApi() {
		inputxml = new DMSInputXml();
		xmlresponse=new DMSXmlResponse();
	}

	public String connectCabinet() {
		return callBroker(inputxml.getConnectCabinetXml(pp.getProperty("cabinetName"), pp.getProperty("username"), pp.getProperty("userpassword"), "", "Y", "0", "S", ""));
	}
	public String disconnectCabinet(String sessionId) {
		String str=inputxml.getDisconnectCabinetXml(pp.getProperty("cabinetName"), sessionId);
		str=callBroker(str);
		logger.info("Cabinet Disconnect Call :"+str);
		return str;
	}
	public String callBroker(String str) {
		String outputxml = null;
		try {
			//logger.info("Cabinet Connect Call :" + str);
			outputxml = DMSCallBroker.execute(str, pp.getProperty("serverIp"), 3333, 0);
		} catch (Exception e) {
			logger.info("Error In DMS Call Broke" + e);
		}
		return outputxml;
	}

	public ArrayList<String> getSessionId() {
		ArrayList<String> al = new ArrayList<String>();
		String sessionId = null;
		String volumeId = null;
		xmlresponse.setXmlString(connectCabinet());
		logger.info("Cabinet Connect :"+xmlresponse);
		if (xmlresponse.getVal("Status").equalsIgnoreCase("0")) {
			sessionId = xmlresponse.getVal("UserDBId");
			volumeId = xmlresponse.getVal("ImageVolumeIndex");
			al.add(sessionId);
			al.add(volumeId);
			logger.info("Session ID :" + sessionId);
			logger.info("Volume ID :" + volumeId);
			return al;
		} else {
			return null;
		}
	}
	public String createFolder(int parentFolderIndex,String folderName,String sessionId) {
		String input = null;
		String output = null;
		input = inputxml.getAddFolderXml(pp.getProperty("cabinetName"), sessionId, String.valueOf(parentFolderIndex),folderName, "", 
				"", "1", "", "", "", "N", "", "", "", "", "", "", "Y", "", "255", "0", "0", "","");
		output=callBroker(input);
		return output;
	}
	
}
