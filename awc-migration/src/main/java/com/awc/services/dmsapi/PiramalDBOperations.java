/**
 * 
 */
package com.awc.services.dmsapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.dbconnection.ConnectPiramalDB;
import com.awc.dto.DocumentData;
//import com.awc.dto.VersionContent;
import com.awc.properties.PropertiesReader;
import com.awc.services.dmsapi.CabinetApi;
import com.newgen.dmsapi.DMSXmlResponse;

/**
 * @author Pratik
 *
 */
public class PiramalDBOperations {
	private DMSXmlResponse xmlresponse;
	private CabinetApi cb;
	private static Properties pp;
	final static Logger logger = Logger.getLogger(PiramalDBOperations.class);
	private ConnectPiramalDB dbCon;
	private String rootFolderQuery = "select Name,FolderIndex from PDBFolder where Name=? and FolderIndex = ?";
	private String generalFolderIdQuery="select name,ParentFolderIndex,FolderIndex from PDBFolder where ParentFolderIndex=?";
	private int flag=0;
	/**
	 * 
	 */
	static {
		pp=PropertiesReader.readPropFile();
	}
	public PiramalDBOperations() {
		dbCon = new ConnectPiramalDB();
		xmlresponse=new DMSXmlResponse();
		cb=new CabinetApi();
	}

	

	public synchronized int validateCreateFolder(DocumentData dd,String sesisonId) {
		return rootFolder(dd,sesisonId);
	}
	
	public int rootFolder(DocumentData dd,String sessionId) {
		
		int data=0;
		flag=0;
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			conn = dbCon.getConnection();
			ps=conn.prepareStatement(rootFolderQuery);
			ps.setString(1, pp.getProperty("rootFolder"));
			ps.setInt(2, Integer.valueOf(pp.getProperty("rootFolderIndex")));
			rs=ps.executeQuery();
			
			while(rs.next()) {
				if(rs.getString(1).equals(pp.getProperty("rootFolder")) && rs.getInt(2)==Integer.valueOf(pp.getProperty("rootFolderIndex"))) {
					flag++;
					data=leadIdFolder(dd,Integer.valueOf(pp.getProperty("rootFolderIndex")),sessionId);
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		}
		finally {
			try {
				rs.close();
				ps.close();
				conn.close();
			}
			catch(SQLException e) {
				logger.error(e);
			}
		}
		return data;
	}
	public int leadIdFolder(DocumentData dd,int parentFolderIndex,String sessionId) {
		int data=0;
		flag=0;
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		//Addition on June 13
		int folderIndex=0;
		try {
			conn = dbCon.getConnection();
			ps=conn.prepareStatement(generalFolderIdQuery);
			logger.info("Parent Folder Index : "+parentFolderIndex);
			ps.setInt(1,parentFolderIndex);
			rs=ps.executeQuery();
			while(rs.next()) {
				if(rs.getString(1).equals(dd.getLeadId())) {
					flag++;
					//Addition on June 13
					folderIndex=rs.getInt(3);
					break;
				}
			}
			if(flag==0) {
				xmlresponse.setXmlString(cb.createFolder(parentFolderIndex, dd.getLeadId(),sessionId));
				data = Integer.parseInt(xmlresponse.getVal("FolderIndex"));
				//Addition on June 13
				//data=documentTypeFolder(dd,data,sessionId);
			}
			if(flag!=0) {
				//Addition on June 13
				//data=documentTypeFolder(dd,rs.getInt(3),sessionId);
				data=folderIndex;
			}
		}
		catch(SQLException e) {
			logger.error(e);
		}
		finally {
			try {
				rs.close();
				ps.close();
				conn.close();
			}
			catch(SQLException e) {
				logger.error(e);
			}
		}
		return data;
	}
	
}
