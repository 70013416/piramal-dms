/**
 * 
 */
package com.awc.dbconnection;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class ConnectPiramalDB {
	private static BasicDataSource bds;
	private static Properties prop;
	private Connection conn;

	static {
		prop = PropertiesReader.readPropFile();
		bds = new BasicDataSource();
		try {
			bds.setDriver((Driver) Class.forName(prop.getProperty("driver")).newInstance());
			bds.setUrl(prop.getProperty("url"));
			bds.setUsername(prop.getProperty("user"));
			bds.setPassword(prop.getProperty("password"));
			bds.setMaxIdle(10);
			bds.setMinIdle(5);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	

	public Connection getConnection() {
		try {
			conn = bds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
}
