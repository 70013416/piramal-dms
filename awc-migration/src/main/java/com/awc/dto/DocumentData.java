/**
 * 
 */
package com.awc.dto;

import java.util.Arrays;

/**
 * @author Pratik
 *
 */
public class DocumentData {

	private String filePath,fileCreationDate,leadId,documentType,document,applicant,userName,docN;
	
	public DocumentData() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param filePath
	 * @param fileCreationDate
	 * @param leadId
	 * @param documentType
	 * @param document
	 * @param applicant
	 * @param userName
	 * @param docN
	 */
	public DocumentData(String filePath, String fileCreationDate, String leadId, String documentType, String document,
			String applicant, String userName, String docN) {
		super();
		this.filePath = filePath;
		this.fileCreationDate = fileCreationDate;
		this.leadId = leadId;
		this.documentType = documentType;
		this.document = document;
		this.applicant = applicant;
		this.userName = userName;
		this.docN = docN;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileCreationDate
	 */
	public String getFileCreationDate() {
		return fileCreationDate;
	}

	/**
	 * @param fileCreationDate the fileCreationDate to set
	 */
	public void setFileCreationDate(String fileCreationDate) {
		this.fileCreationDate = fileCreationDate;
	}

	/**
	 * @return the leadId
	 */
	public String getLeadId() {
		return leadId;
	}

	/**
	 * @param leadId the leadId to set
	 */
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	/**
	 * @return the documentType
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	/**
	 * @return the document
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(String document) {
		this.document = document;
	}

	/**
	 * @return the applicant
	 */
	public String getApplicant() {
		return applicant;
	}

	/**
	 * @param applicant the applicant to set
	 */
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the docN
	 */
	public String getDocN() {
		return docN;
	}

	/**
	 * @param docN the docN to set
	 */
	public void setDocN(String docN) {
		this.docN = docN;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicant == null) ? 0 : applicant.hashCode());
		result = prime * result + ((docN == null) ? 0 : docN.hashCode());
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((documentType == null) ? 0 : documentType.hashCode());
		result = prime * result + ((fileCreationDate == null) ? 0 : fileCreationDate.hashCode());
		result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result + ((leadId == null) ? 0 : leadId.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentData other = (DocumentData) obj;
		if (applicant == null) {
			if (other.applicant != null)
				return false;
		} else if (!applicant.equals(other.applicant))
			return false;
		if (docN == null) {
			if (other.docN != null)
				return false;
		} else if (!docN.equals(other.docN))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (documentType == null) {
			if (other.documentType != null)
				return false;
		} else if (!documentType.equals(other.documentType))
			return false;
		if (fileCreationDate == null) {
			if (other.fileCreationDate != null)
				return false;
		} else if (!fileCreationDate.equals(other.fileCreationDate))
			return false;
		if (filePath == null) {
			if (other.filePath != null)
				return false;
		} else if (!filePath.equals(other.filePath))
			return false;
		if (leadId == null) {
			if (other.leadId != null)
				return false;
		} else if (!leadId.equals(other.leadId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DocumentData [filePath=" + filePath + ", fileCreationDate=" + fileCreationDate + ", leadId=" + leadId
				+ ", documentType=" + documentType + ", document=" + document + ", applicant=" + applicant
				+ ", userName=" + userName + ", docN=" + docN + "]";
	}
	
}
