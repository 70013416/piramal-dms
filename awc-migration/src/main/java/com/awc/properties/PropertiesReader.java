/**
 * @author Pratik
 *
 */
package com.awc.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Pratik
 *
 */
public class PropertiesReader {

	/**
	 * @param args
	 */
	public static Properties readPropFile() {
		Properties prop = new Properties();
		try (InputStream input = PropertiesReader.class.getClassLoader().getResourceAsStream("db.properties")) {
            if (input == null) {
                System.out.println("Sorry, unable to find db.properties");
            }
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
		return prop;
	}
}

