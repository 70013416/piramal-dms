/**
 * 
 */
package com.awc.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.compression.ReduceSize;
import com.awc.dto.DocumentData;
import com.awc.properties.PropertiesReader;
import com.awc.services.dmsapi.CabinetApi;
import com.awc.services.dmsapi.UploadDocument;
import com.itextpdf.text.DocumentException;
import com.newgen.dmsapi.DMSXmlResponse;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

/**
 * @author Pratik
 *
 */
public class ReadCsv {
	
	final static Logger logger = Logger.getLogger(ReadCsv.class);
	private String tempLocation="D:\\Newgen\\Server\\TempLocation\\Compression";
	private Map<String, String> mapping = new HashMap<String, String>();
	private CabinetApi api;
	private DMSXmlResponse xmlresponse;
	private Properties pp;
	public ReadCsv() {
		api=new CabinetApi();
		xmlresponse=new DMSXmlResponse();
		pp = PropertiesReader.readPropFile();
	}

	public void readCsv() {
		//System.out.println("Branch "+branch);
		mapping.put("FilePath", "filePath");
		mapping.put("FileCreationDate", "fileCreationDate");
		mapping.put("LeadId", "leadId");
		mapping.put("DocumentType", "documentType");
		mapping.put("DocumentName", "document");
		mapping.put("Applicant", "applicant");
		HeaderColumnNameTranslateMappingStrategy strategy = new HeaderColumnNameTranslateMappingStrategy();
		strategy.setType(DocumentData.class);
		strategy.setColumnMapping(mapping);
		//String csvFilename = "data.csv";
		CSVReader csvReader;
		try {
			csvReader = new CSVReader(new FileReader(pp.getProperty("csvLocation")));
			CsvToBean csv = new CsvToBean();
			List list = csv.parse(strategy, csvReader);
			Iterator itr=list.iterator();
		DocumentData dd=null;
		xmlresponse.setXmlString(api.connectCabinet());
		ReduceSize rs=new ReduceSize();
		while(itr.hasNext()) {
			UploadDocument ud=null;
			ud=new UploadDocument();
			dd=(DocumentData)itr.next();
			dd.setLeadId(dd.getLeadId().trim());
			dd.setUserName(pp.getProperty("uploadedBy"));
			logger.info("Document Data - >"+dd);
			String src=dd.getFilePath();
			File flag=new File(src);
			if(flag.exists()) {
				try {
				String target=tempLocation+dd.getFilePath().substring(dd.getFilePath().lastIndexOf("\\"), dd.getFilePath().length());
				rs.manipulatePdf(dd.getFilePath(), target);
				dd.setFilePath(target);
				ud.upload(dd, xmlresponse.getVal("UserDBId"));
				
				logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
				logger.info("File Source : "+src);
				Files.delete(Paths.get(src));
				logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
				}
				catch(DocumentException | IOException e) {
					logger.error(e.getStackTrace());
					logger.error("Issue with this PDF on File Compression :"+dd.getFilePath());
				}
				catch(Exception e) {
					logger.error(e.getStackTrace());
				}
			}
			
			else {
				logger.error("File does not exist :"+flag.getAbsolutePath()+"--> "+flag.getName());
				flag=null;
			}
		}
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getStackTrace());
		}
	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws DocumentException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args){
		ReadCsv csvx=new ReadCsv();
		csvx.readCsv();
	}

}
