<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.awc.dao.*" %>
<%@ page import="com.awc.dao.impl.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#work-in-progress{
background: #00000085;
    height: 100vh;
    text-align: center;
    padding-top: 50vh;
    position: absolute;
    width: 100%;
    color: #fff;
    font-size: 22px;
    top: 0;
    margin: 0px -8px;
}
</style>
</head>
<body>

<h2>Hi User,</h2>
<p>Please Click on one of the Tab to Download Documents</p>

<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'DateWiseDownload')">Date Wise Download</button>
  <button class="tablinks" onclick="openCity(event, 'DataWiseDownload')">Data Wise Download</button>
  
</div>

<div id="DateWiseDownload" class="tabcontent">
  <form action="./DownloadDocumetsDateWise" method="post">
			<%
				//out.println("Hi Code !!");
				Cookie cookie = null;
				Cookie[] cookies = null;
				cookies = request.getCookies();
				for (int i = 0; i < cookies.length; i++) {
					cookie = cookies[i];
					response.addCookie(cookie);
				}
			%>
			<table>
				<tr>
					<td><h3>Date Wise Download</h3></td>
				</tr>
				<tr>
					<td>From Date</td>
					<td><input type="date" name="fromDate" required></td>
				</tr>
				<tr>
					<td>To Date</td>
					<td><input type="date" name="toDate" required></td>
				</tr>
			</table>
			<input type="submit" value="Download" onclick="showMessage()">
		</form>
</div>

<div id="DataWiseDownload" class="tabcontent">
 		<form action="./DownloadDocumetsFilterWise" method="post">
			<%
				Cookie cookief = null;
				Cookie[] cookiesf = null;
				cookiesf = request.getCookies();
				for (int i = 0; i < cookiesf.length; i++) {
					cookief = cookiesf[i];
					response.addCookie(cookief);

				}
			%>
			<table>
				<tr>
					<td><h3>Data Wise Download</h3></td>
				</tr>
				<tr>
					<td>Enter Lead Ids</td>
					<td><input type="text" name="leadIds" required></td>
				</tr>
				 <tr>
					<td>Enter Document Type</td>
					<td><select name="documentType">
							<option></option>
							<!-- <option>AWC</option>
							<option>NILE</option> -->
							<%
							
							IDownloadDocumentsDAO dao=new DownloadDocumentImpl();
							for(String str:dao.getDocumentTypes()){
							
							//System.out.println("From JSP : "+str);
							%>
								<option><%=str%></option>
							<%}%>
						</select>
					</td>
				</tr>
				<tr>
					<td>Enter Document</td>
					<td><select name="document">
							<option></option>
							<!-- <option>AWC</option>
							<option>NILE</option> -->
							<%
							for(String str:dao.getDocuments()){
							
							//System.out.println("From JSP : "+str);
							%>
								<option><%=str%></option>
							<%}%>
						</select>
					</td>
				</tr> 
				<tr>
					<td>Enter Applicant</td>
					<td><select name="applicant">
							<option></option>
							<!-- <option>AWC</option>
							<option>NILE</option> -->
							<%
							for(String str:dao.getApplicant()){
							
							//System.out.println("From JSP : "+str);
							%>
								<option><%=str%></option>
							<%}%>
						</select>
					</td>
				</tr> 
			</table>
			<input type="submit" value="Download"  onclick="showMessage()">
		</form>
</div>
<div id="work-in-progress" style="display:none;" >
<p>Please wait !! Your data is downloading..</p>
</div>



<script> 
function showMessage() {	
	  var x = document.getElementById("work-in-progress");	
	  x.style.display = "block";
	  }

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
   
</body>
</html> 
