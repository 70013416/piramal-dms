package com.awc.service.dmsapi;
/**
 * 
 */

import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.properties.PropertiesReader;
import com.newgen.dmsapi.DMSCallBroker;
import com.newgen.dmsapi.DMSInputXml;

/**
 * @author Pratik
 *
 */
public class CabinetApi {
	final static Logger logger = Logger.getLogger(CabinetApi.class);
	private DMSInputXml inputxml;
	private static Properties pp;
	static {
		pp=PropertiesReader.readPropFile();
	}
	public CabinetApi() {
		inputxml = new DMSInputXml();
		
	}
	
	public String connectCabinet(String userName,String userPassword) {
		return callBroker(inputxml.getConnectCabinetXml(pp.getProperty("cabinetName"), userName, userPassword, "", "Y", "0", "S", ""));
	}
	public String disconnectCabinet(String sessionId) {
		String str=inputxml.getDisconnectCabinetXml(pp.getProperty("cabinetName"), sessionId);
		str=callBroker(str);
		logger.info("Cabinet Disconnect Call :"+str);
		return str;
	}
	public static String callBroker(String str) {
		String outputxml = null;
		try {
			outputxml = DMSCallBroker.execute(str, pp.getProperty("serverIp"), 3333, 0);
		} catch (Exception e) {
			logger.info("Error In DMS Call Broke" + e);
		}
		return outputxml;
	}

	
	
	
}
