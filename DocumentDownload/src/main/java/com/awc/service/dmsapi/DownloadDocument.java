/**
 * 
 */
package com.awc.service.dmsapi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.dao.impl.DownloadDocumentImpl;
import com.awc.properties.PropertiesReader;

import ISPack.CPISDocumentTxn;
import ISPack.ISUtil.JPISException;
import Jdts.DataObject.JPDBString;

/**
 * @author Pratik
 *
 */

public class DownloadDocument {
	final static Logger logger = Logger.getLogger(DownloadDocumentImpl.class);
	private ByteArrayOutputStream streamBuff;
	private JPDBString siteName;
	private static Properties pp;
	static {
		pp = PropertiesReader.readPropFile();
	}

	/**
	 * 
	 */
	public DownloadDocument() {
		siteName = new JPDBString();
	}

	public void documentDownload(String filePath,String fName,String fExt,int imageIndex,int documentIndex, String sessionId,int x) {
		try {
			File f=null;
			f=new File(filePath+fName+"."+fExt.toLowerCase());
			if(f.exists()) {
				f=new File(filePath+fName+ x +"." +fExt.toLowerCase());
			}
			streamBuff = new ByteArrayOutputStream();
			FileOutputStream fout=null;
			fout =new FileOutputStream(f);
			CPISDocumentTxn.GetDocInFile_MT(null, pp.getProperty("serverIp"), (short) 3333,
					pp.getProperty("cabinetName"), (short) 1, (short) 1, imageIndex, sessionId, streamBuff, siteName);
		
			streamBuff.writeTo(fout);
			streamBuff.flush();
			streamBuff.close();
			fout.close();
			String xmlString="<?xml version=\"1.0\"?><NGOGenerateAuditTrail_Input><Option>NGOGenerateAuditTrail</Option>"+
							 "<CabinetName>"+pp.getProperty("cabinetName")+"</CabinetName>"+
							 "<UserDBId>"+sessionId+"</UserDBId>"+
							 "<Action>"+
							 "<ActionId>306</ActionId>"+
							 "<Category>D</Category>"+
							 "<ActiveObjectId>"+documentIndex+"</ActiveObjectId>"+
							 "<ActiveObjectType>D</ActiveObjectType>"+
							 "<SubsdiaryObjectId>-1</SubsdiaryObjectId>"+
							 "<SubsdiaryObjectType></SubsdiaryObjectType>"+
							 "<Comment>"+fName+"</Comment>"+
							 "</Action>"+
							 "</NGOGenerateAuditTrail_Input>";
			logger.info(CabinetApi.callBroker(xmlString));
			
		} catch (JPISException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
