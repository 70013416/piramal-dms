package com.awc.dao;

import java.util.List;

public interface IDownloadDocumentsDAO {
	public void downloadDocumentsDateWise(String path,String sessionId,String fromDate,String toDate);
	public void downloadDocumentsFilterWise(String path,String sessionId,String leadIds,String documentType,String document,String applicant);
	public List<String> getDocumentTypes();
	public List<String> getDocuments();
	public List<String> getApplicant();
}
