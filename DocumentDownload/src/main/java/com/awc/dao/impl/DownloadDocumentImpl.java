/**
 * 
 */
package com.awc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.awc.dao.IDownloadDocumentsDAO;
import com.awc.dbconnection.ConnectPiramalDB;
import com.awc.properties.PropertiesReader;
import com.awc.service.dmsapi.CabinetApi;
import com.awc.service.dmsapi.DownloadDocument;

/**
 * @author Pratik
 *
 */
public class DownloadDocumentImpl implements IDownloadDocumentsDAO {
	final static Logger logger = Logger.getLogger(DownloadDocumentImpl.class);
	private CabinetApi apix;
	private ArrayList<String> al;
	private Connection conn;
	private PreparedStatement datePs, docType;
	private ConnectPiramalDB dbconn;
	private ResultSet dateRs, docTypeRs;
	private Properties pp;
	private String dateWiseQuery = "select * from PDBDocument where ImageIndex>0 and CreatedDateTime between ? and ?";

	public DownloadDocumentImpl() {
		apix=new CabinetApi();
		pp = PropertiesReader.readPropFile();
		al = new ArrayList<String>();
		dbconn = new ConnectPiramalDB();
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.awc.dao.DownloadDocumentsDAO#downloadDocuments(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void downloadDocumentsDateWise(String path, String sessionId, String fromDate, String toDate) {
		int xxx=0;
		DownloadDocument dd = null;
		dd = new DownloadDocument();
		try {
			//logger.info("SessionIdx : " + sessionId);
			// logger.info("1 :");
			conn = dbconn.getConnection();
			// logger.info("2 :");
			logger.info("From Date : " + fromDate + "\t\t\t::" + "To Date : " + toDate);
			datePs = conn.prepareStatement(dateWiseQuery);
			datePs.setString(1, fromDate + " 00:00:00:0");
			datePs.setString(2, toDate + " 23:59:59:0");
			dateRs = datePs.executeQuery();
			while (dateRs.next()) {
				
				logger.info(dateRs.getString(4) + " :: " + dateRs.getString(40) + " :: " + dateRs.getInt(15));
				dd.documentDownload(path, dateRs.getString(4), dateRs.getString(40), dateRs.getInt(15),dateRs.getInt(1), sessionId,++xxx);
			}
		} catch (SQLException e) {
			System.err.println("Error in SQL : " + e);
			logger.error(e.getMessage());
		} finally {
			try {
				dateRs.close();
				//logger.info("1");
				datePs.close();
				//logger.info("2");
				conn.close();
				//logger.info("3");
				logger.debug(apix.disconnectCabinet(sessionId));
				logger.info("DownloadDocumentsDateWise Finally Block Closed !");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}

	}

	@Override
	public void downloadDocumentsFilterWise(String path, String sessionId, String leadIds, String documentType,
			String document, String applicant) {
		int xxx=0;
		DownloadDocument dd = null;
		dd = new DownloadDocument();
		logger.info("Path : " + path + "\t SessionId : " + sessionId + " \tLeadid : " + leadIds);
		conn = dbconn.getConnection();

		String arr[] = leadIds.split(",");

		String query = "";

		for (String myStr : arr) {
			query += "'" + myStr + "',";
		}
		
		// Document Type
		logger.info("Document Type Null Check : " + documentType.equals("null"));
		String documentTypeQuery = null;
		if (documentType.equals("") || documentType.equals("null")) {
			logger.info("DocumentType Blank");
			documentTypeQuery = "";
		} else {
			logger.info("DocumentType Data Got");
			documentTypeQuery = " and " + pp.getProperty("documentTypeField") + " = " + "'" + documentType + "'";
		}
		// Document
		String documentQuery = null;
		if (document.equals("") || document.equals("null")) {
			logger.info("Document Blank");
			documentQuery = "";
		} else {
			logger.info("Document Data Got");
			documentQuery = " and " + pp.getProperty("documentField") + " = " + "'" + document + "'";
		}

		// Applicant
		logger.info("Null Check : " + applicant.equals("null"));
		String applicantQuery = null;
		if (applicant.equals("") || applicant.equals("null")) {
			logger.info("Applicant Blank");
			applicantQuery = "";
		} else {
			logger.info("Applicant Data Got");
			applicantQuery = " and " + pp.getProperty("applicantField") + " = " + "'" + applicant + "'";
		}
		// logger.info("Applicant Query : "+applicantQuery);
		query = query.substring(0, query.length() - 1);
		
			String otherParameters = documentTypeQuery + documentQuery + applicantQuery;
			String leadQuery = "select * from PDBDocument where ImageIndex > 0  and DocumentIndex in (select FoldDocIndex from ddt_3 where Field_9 in ("
					+ query + ")" + otherParameters + ")";
			logger.info("Final  Query : "+leadQuery);
			Statement leadIdstmt=null;
			ResultSet rs = null;
			try {	

			leadIdstmt = conn.createStatement();
			rs= leadIdstmt.executeQuery(leadQuery);
			while (rs.next()) {
				
				dd.documentDownload(path, rs.getString(4), rs.getString(40), rs.getInt(15),rs.getInt(1), sessionId,++xxx);
			}
			logger.info("Lead Query : " + leadQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
			
				if(rs!=null) {
					rs.close();
				}
				leadIdstmt.close();
				conn.close();
				logger.debug(apix.disconnectCabinet(sessionId));
				logger.info("DownloadDocumentsFilterWise Finally Block Closed !");
				
			}
			catch(SQLException e) {
				logger.error(e.getMessage());
			}
		}
	}
	//Distinct For Drop downs
	@Override
	public List<String> getDocumentTypes() {
		al.clear();
		conn = dbconn.getConnection();
		try {
			docType = conn.prepareStatement(pp.getProperty("documentType"));
			docTypeRs = docType.executeQuery();
			while (docTypeRs.next()) {
				al.add(docTypeRs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				docTypeRs.close();
				docType.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return al;
	}
	//Distinct For Drop downs
	@Override
	public List<String> getDocuments() {
		al.clear();
		conn = dbconn.getConnection();
		try {
			docType = conn.prepareStatement(pp.getProperty("document"));
			docTypeRs = docType.executeQuery();
			while (docTypeRs.next()) {
				al.add(docTypeRs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				docTypeRs.close();
				docType.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return al;
	}
	//Distinct For Drop downs
	@Override
	public List<String> getApplicant() {
		al.clear();
		conn = dbconn.getConnection();
		try {
			docType = conn.prepareStatement(pp.getProperty("applicant"));
			docTypeRs = docType.executeQuery();
			while (docTypeRs.next()) {
				al.add(docTypeRs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				docTypeRs.close();
				docType.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return al;
	}

}
