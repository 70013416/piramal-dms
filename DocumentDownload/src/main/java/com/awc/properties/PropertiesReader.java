/**
 * @author Pratik
 *
 */
package com.awc.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Pratik
 *
 */
public class PropertiesReader {

	/**
	 * @param args
	 */
	public static Properties readPropFile() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String filename = "db.properties";
			input = PropertiesReader.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
				return null;
			}
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
}

