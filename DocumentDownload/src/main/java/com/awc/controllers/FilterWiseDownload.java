/**
 * 
 */
package com.awc.controllers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.awc.dao.IDownloadDocumentsDAO;
import com.awc.dao.impl.DownloadDocumentImpl;
import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class FilterWiseDownload extends HttpServlet {
	private static Properties pp;
	static {
		pp = PropertiesReader.readPropFile();
	}
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		Cookie cookie = null;
		Cookie[] cookies = null;
		cookies = req.getCookies();
		PrintWriter out=res.getWriter();
		Date date = new Date();
		File dir = null;

		File userDir = null;

		IDownloadDocumentsDAO dao = null;
		dao = new DownloadDocumentImpl();
		res.setContentType("text/html");

		for (int x = 0; x < cookies.length; x++) {
			cookie = cookies[x];
			if (cookie.getName().equals("user")) {
				userDir = new File(pp.getProperty("otherFilter") + File.separatorChar + cookie.getValue());
				userDir.mkdir();
				dir = new File(userDir.getAbsolutePath() + File.separatorChar + dateFormat.format(date));

			}
		}
		if (dir.mkdir()) {
			System.out.println("directory created with current date");
		} else {
			System.err.println("Issue in creating folder");
		}

		System.out.println("Lead Ids From Servlet : " + req.getParameter("leadIds"));
		System.out.println("Document Type : " + req.getParameter("documentType"));
		System.out.println("Document : " + req.getParameter("document"));
		System.out.println("Applicant : " + req.getParameter("applicant"));

		for (int i = 0; i < cookies.length; i++) {
			cookie = cookies[i];
			if (cookie.getName().equals("sid")) {
				dao.downloadDocumentsFilterWise(dir.getAbsolutePath() + File.separatorChar, cookie.getValue(),
						req.getParameter("leadIds"),req.getParameter("documentType"),req.getParameter("document"),req.getParameter("applicant"));
			}
		}
		out.print("Download Complete");
	}
}
