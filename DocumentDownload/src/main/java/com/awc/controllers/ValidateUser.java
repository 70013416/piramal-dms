/**
 * 
 */
package com.awc.controllers;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.awc.service.dmsapi.CabinetApi;
import com.newgen.dmsapi.DMSXmlResponse;

/**
 * @author Pratik
 *
 */
public class ValidateUser extends HttpServlet {
	//private PropertiesReader pr;
	private CabinetApi api=null;
	private DMSXmlResponse response;
	final static Logger logger = Logger.getLogger(ValidateUser.class);
	/**
	 * 
	 */
	public ValidateUser() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void doPost(HttpServletRequest req,HttpServletResponse res) {
		try {
			PrintWriter out=res.getWriter();
			res.setContentType("text/html");
			/*pr=null;
			pr=new PropertiesReader();*/
			//Properties prop=PropertiesReader.readPropFile();
			response=new DMSXmlResponse();
			String userName=req.getParameter("username");
			String userPassword=req.getParameter("userpassword");
			api=new CabinetApi();
			
			//logger.info("OutPut Response : "+api.connectCabinet(userName, userPassword));
			String str=null;
			str=api.connectCabinet(userName, userPassword);
			logger.info("OutPut Response : "+str);
			response.setXmlString(str);
			if (response.getVal("Status").equalsIgnoreCase("0")) {
				Cookie user=null;
				user=new Cookie("user", userName);
				Cookie sessionId=null;
				sessionId=new Cookie("sid", response.getVal("UserDbId"));
				res.addCookie(sessionId);
				res.addCookie(user);
				RequestDispatcher rd=req.getRequestDispatcher("pages/downloadPage.jsp");
				rd.include(req, res);
			}
			else {
				
				out.print("<center><h2>"+response.getVal("Error")+"</h2></center>");
				RequestDispatcher rd=req.getRequestDispatcher("index.jsp");
				rd.include(req, res);
				
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
