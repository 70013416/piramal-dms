/**
 * 
 */
package com.awc.controllers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.awc.dao.IDownloadDocumentsDAO;
import com.awc.dao.impl.DownloadDocumentImpl;
import com.awc.properties.PropertiesReader;

/**
 * @author Pratik
 *
 */
public class DateWiseDownload extends HttpServlet {
	final static Logger logger = Logger.getLogger(DateWiseDownload.class);
	private static Properties pp;
	static {
		pp = PropertiesReader.readPropFile();
	}
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		Cookie cookie = null;
		Cookie[] cookies = null;
		cookies = req.getCookies();
		PrintWriter out=res.getWriter();
		Date date = new Date();
		File dir = null;
		
		File userDir=null;
		
		IDownloadDocumentsDAO dao = null;
		dao = new DownloadDocumentImpl();
		res.setContentType("text/html");

		for(int x=0;x<cookies.length;x++) {
			cookie=cookies[x];
			if(cookie.getName().equals("user")) {
				userDir=new File(pp.getProperty("dateFilter") + File.separatorChar +cookie.getValue());
				userDir.mkdir(); 
				dir = new File(userDir.getAbsolutePath() + File.separatorChar + dateFormat.format(date));
				
			}
		}
		
		if (dir.mkdir()) {
			logger.info("directory created with current date");
		} else {
			logger.error("Issue in creating folder");
		}
		
		String toDate = req.getParameter("toDate");
		String fromDate = req.getParameter("fromDate");


		for (int i = 0; i < cookies.length; i++) {
			cookie = cookies[i];
			if (cookie.getName().equals("sid")) {
				dao.downloadDocumentsDateWise(dir.getAbsolutePath() + File.separatorChar, cookie.getValue(), fromDate, toDate);
				//logger.info("Servlet Call Cookie Name :"+cookie.getName()+" -> Cookie Value :"+cookie.getValue());
			}
		}
		out.print("Download Complete");
	}
}
