/**
 * 
 */
package com.awcsoftware.dmsapi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.awcsoftware.base64.EncryptionPiramal;
import com.awcsoftware.dto.BinaryData;
import com.awcsoftware.services.PropertiesReader;
import ISPack.CPISDocumentTxn;
import ISPack.ISUtil.JPISException;
import Jdts.DataObject.JPDBString;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DownloadDocument {
	@Autowired
	private EncryptionPiramal esk;
	@Autowired
	private ByteArrayOutputStream streamBuff;
	@Autowired
	private JPDBString siteName;
	@Autowired
	private ApplicationContext ctx;


	public BinaryData documentDownload(int id, String sessionId) {
		byte[] data = null;
		BinaryData bd=ctx.getBean(BinaryData.class);
		try {
			CPISDocumentTxn.GetDocInFile_MT(null, PropertiesReader.getProp().getProperty("serverIp"), (short) 3333,
					PropertiesReader.getProp().getProperty("cabinetName"), (short) 1, (short) 1, id, sessionId, streamBuff, siteName);
			data = esk.encodeData(streamBuff.toByteArray());
			streamBuff.flush();
			streamBuff.close();
			bd.setImageIndex(id);
			bd.setData(ArrayUtils.toObject(data));
		} catch (JPISException e) {
			bd.setError("Document not found");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bd;


	}
}
