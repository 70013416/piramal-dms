package com.awcsoftware.dmsapi;
import java.io.File;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.awcsoftware.dto.ISIndexData;
import com.awcsoftware.dto.VersionRequest;
import com.awcsoftware.dto.VersionResponse;
import com.awcsoftware.services.PropertiesReader;
import com.newgen.dmsapi.DMSXmlResponse;
import ISPack.CPISDocumentTxn;
import ISPack.ISUtil.JPDBRecoverDocData;
import ISPack.ISUtil.JPISException;
import ISPack.ISUtil.JPISIsIndex;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DocumentVersioning {
	final static Logger logger = Logger.getLogger(DocumentVersioning.class);
	@Autowired
	private JPDBRecoverDocData docDBData;
	@Autowired
	private JPISIsIndex newIsIndex;
	@Autowired
	private CabinetAPI api;
	@Autowired
	private ApplicationContext ctx;
	private DMSXmlResponse response;
	private String isIndexNew;
	
	public VersionResponse checkin(VersionRequest req,String sessionId,ISIndexData listData,File f) {
		logger.info("Entered checkin method");
		VersionResponse vr = ctx.getBean(VersionResponse.class);
		try {
				CPISDocumentTxn.AddDocument_MT(null, PropertiesReader.getProp().getProperty("serverIp"),
						Short.parseShort(PropertiesReader.getProp().getProperty("jtsPort")), PropertiesReader.getProp().getProperty("cabinetName"),
						Short.parseShort("1"), PropertiesReader.getProp().getProperty("fileLocation") + FilenameUtils.getName(f.getName()), docDBData, "1",
						newIsIndex);
				isIndexNew = newIsIndex.m_nDocIndex + "#" + newIsIndex.m_sVolumeId;
					
				String checkoutinputXml = "<?xml version=\"1.0\"?><NGOCheckinCheckoutExt_Input><Option>NGOCheckinCheckoutExt</Option><CabinetName>"
						+ PropertiesReader.getProp().getProperty("cabinetName") + "</CabinetName><UserDBId>" + sessionId
						+ "</UserDBId><CurrentDateTime></CurrentDateTime><LimitCount>1000</LimitCount><CheckInOutFlag>Y</CheckInOutFlag><SupAnnotVersion>N</SupAnnotVersion><Documents><Document><DocumentIndex>"
						+ listData.getDocumentIndex() + "</DocumentIndex><ISIndex>" + isIndexNew
						+ "</ISIndex></Document></Documents></NGOCheckinCheckoutExt_Input>";
					
				response=api.callBroker(checkoutinputXml);
					
				if (Integer.parseInt(response.getVal("Status")) == 0) {
					String checkininputXml = "<?xml version=\"1.0\"?><NGOCheckinCheckoutExt_Input><Option>NGOCheckinCheckoutExt</Option><CabinetName>"
							+ PropertiesReader.getProp().getProperty("cabinetName") + "</CabinetName><UserDBId>" + sessionId
							+ "</UserDBId><CurrentDateTime></CurrentDateTime><LimitCount>1000</LimitCount><CheckInOutFlag>N</CheckInOutFlag><SupAnnotVersion>N</SupAnnotVersion><Documents><Document><DocumentIndex>"
							+ listData.getDocumentIndex() + "</DocumentIndex><ISIndex>" + isIndexNew
							+ "</ISIndex><VersionComment>" + req.getComment()
							+ "</VersionComment><DocumentType>N</DocumentType><NoOfPages>" + 1
							+ "</NoOfPages><DocumentSize>" + f.length() + "</DocumentSize><CreatedByAppName>" + FilenameUtils.getExtension(f.getName())
							+ "</CreatedByAppName></Document></Documents></NGOCheckinCheckoutExt_Input>";
					response=api.callBroker(checkininputXml);
						
					if (Integer.parseInt(response.getVal("Status")) == 0) {
						String propertyChange = "<?xml version=\"1.0\"?><NGOChangeDocumentProperty_Input><Option>NGOChangeDocumentProperty</Option><CabinetName>"
								+ PropertiesReader.getProp().getProperty("cabinetName") + "</CabinetName><UserDBId>" + sessionId
								+ "</UserDBId><Document><DocumentIndex>" + listData.getDocumentIndex()
								+ "</DocumentIndex><NoOfPages>1</NoOfPages><DocumentName>"
								+ response.getVal("DocumentName") + "</DocumentName><AccessDateTime>"
								+ response.getVal("AccessDateTime") + "</AccessDateTime><ISIndex>" + isIndexNew
								+ "</ISIndex><VersionComment>Revised</VersionComment><DocumentType>" + UploadDocument.getDocType(FilenameUtils.getExtension(f.getName()))
								+ "</DocumentType><DocumentSize>" + f.length()
								+ "</DocumentSize><DataDefinition></DataDefinition><RetainAnnotation>N</RetainAnnotation></Document></NGOChangeDocumentProperty_Input>";
								response=api.callBroker(propertyChange);
							
							vr.setDocId(Integer.parseInt(response.getVal("DocumentIndex")));
							vr.setStatus(Integer.parseInt(response.getVal("Status")));
							vr.setVerNo(Float.parseFloat(response.getVal("DocumentVersionNo")));
							String indexParse = response.getVal("ISIndex");
							indexParse = indexParse.substring(0, indexParse.indexOf("#"));
							vr.setDocumentIndex(Integer.parseInt(indexParse));
						}
					}
				} catch (JPISException e) {
					logger.info(e);
				}
			
		return vr;
	}
	
}
