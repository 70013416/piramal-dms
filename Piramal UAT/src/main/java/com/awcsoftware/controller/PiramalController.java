package com.awcsoftware.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.awcsoftware.dto.BinaryData;
import com.awcsoftware.dto.CaseData;
import com.awcsoftware.dto.DocumentData;
import com.awcsoftware.dto.MetaDataRequest;
import com.awcsoftware.dto.UpdateMetaDataRequest;
import com.awcsoftware.dto.UploadResponse;
import com.awcsoftware.dto.UserData;
import com.awcsoftware.dto.VersionContent;
import com.awcsoftware.dto.VersionRequest;
import com.awcsoftware.dto.VersionResponse;
import com.awcsoftware.services.OmnidocsServices;

@RestController
@RequestMapping(value="/webapi/piramalservices")
public class PiramalController {
	@Autowired
	private ApplicationContext ctx;
	@GetMapping()
	public String sampleData() {
		return "Piramal Omnidocs WebServices Up & Running";
	}
	
	@GetMapping(value = "{documentId}")
	@ResponseBody
	public VersionContent getDocumentIndex(@PathVariable(name = "documentId") int docId) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.getDocumentIndex(docId);
	}
	
	@PostMapping(value = "getsessionid")
	@ResponseBody
	public String getSessionId(@RequestBody UserData ud) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.getSessionId(ud);
	}
	
	@PostMapping(value = "documentUpload")
	@ResponseBody
	public List<UploadResponse> documentUpload(@RequestBody ArrayList<DocumentData> dd, @RequestHeader("sessionid") String sessionId) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.uploadDocument(dd, sessionId);
	}
	
	@PostMapping(value = "caseUpload")
	@ResponseBody
	public List<UploadResponse> caseUpload(@RequestBody ArrayList<CaseData> cd,@RequestHeader("sessionid") String sessionId){
		System.out.println("SessionId : "+sessionId);
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.caseUpload(cd, sessionId);
		
	}
	
	@PostMapping(value = "documentdownload")
	@ResponseBody
	public List<BinaryData> documentDownload(@RequestHeader("sessionid") String sessionId,@RequestHeader("documents")String... documents) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.downloadDocuments(sessionId, documents);
	}
	
	@PostMapping(value="validateSession")
	@ResponseBody
	public String validateSessionId(@RequestBody String sessionId) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.validateSessionId(sessionId);
	}
	
	
	@PutMapping(value = "{documentId}")
	@ResponseBody
	public VersionResponse versionservice(@RequestBody VersionRequest vr,@PathVariable("documentId") String documentId,@RequestHeader("sessionid") String sessionId) {
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return services.versionService(sessionId, vr,documentId);
	}

	/*
	 * @PostMapping(value = "getDocumentMetaData",produces =
	 * MediaType.APPLICATION_JSON_VALUE)
	 * 
	 * @ResponseBody public ResponseEntity<Object>
	 * getDocumentMetaData(@RequestHeader("sessionid") String
	 * sessionId,@RequestHeader("leadIds") String... leadId){ OmnidocsServices
	 * services=ctx.getBean(OmnidocsServices.class); return new
	 * ResponseEntity<>(services.getDocumentMetaData(sessionId, leadId).toString(),
	 * HttpStatus.OK); }
	 * 
	 */ 
	
	@PostMapping(value = "getDocumentMetaData",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getDocumentMetaData(@RequestHeader("sessionid") String sessionId,@RequestBody MetaDataRequest metaDataRequest ){
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		return new ResponseEntity<>(services.getDocumentMetaData(sessionId, metaDataRequest).toString(), HttpStatus.OK);
	}
	
	@PostMapping(value = "updateDocumentMetaData",produces = MediaType.APPLICATION_JSON_VALUE,consumes =MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public ResponseEntity<Object> updateDocumentMetaData(@RequestHeader("sessionid") String sessionId, @RequestBody ArrayList<UpdateMetaDataRequest> updateMetaDataRequest ){
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		System.out.println(updateMetaDataRequest);
		return new ResponseEntity<>(services.updateDocumentMetaData(sessionId, updateMetaDataRequest), HttpStatus.OK);
	}
	
	@PostMapping(value = "getLeads",produces = MediaType.APPLICATION_JSON_VALUE,consumes =MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public ResponseEntity<Object> getLeadsPennant(@RequestHeader("sessionid") String sessionId, @RequestBody ArrayList<UpdateMetaDataRequest> updateMetaDataRequest ){
		OmnidocsServices services=ctx.getBean(OmnidocsServices.class);
		System.out.println(updateMetaDataRequest);
		return new ResponseEntity<>(services.updateDocumentMetaData(sessionId, updateMetaDataRequest), HttpStatus.OK);
	}

}

