package com.awcsoftware.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.awcsoftware.dto.GeneralFolder;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GeneralFolderMapper implements RowMapper<GeneralFolder>{

	@Override
	public GeneralFolder mapRow(ResultSet rs, int rowNum) throws SQLException {
		GeneralFolder gf=new GeneralFolder();
		gf.setName(rs.getString("name"));
		gf.setParentFolderIndex(rs.getInt("ParentFolderIndex"));
		gf.setFolderIndex(rs.getInt("FolderIndex"));
		return gf;
	}

}
