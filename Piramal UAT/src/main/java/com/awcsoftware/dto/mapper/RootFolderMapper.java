package com.awcsoftware.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.awcsoftware.dto.RootFolder;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RootFolderMapper implements RowMapper<RootFolder> {

	@Override
	public RootFolder mapRow(ResultSet rs, int rowNum) throws SQLException {
		RootFolder rc=new RootFolder();
		rc.setName(rs.getString("Name"));
		rc.setFolderIndex(rs.getInt("FolderIndex"));
		return rc;
	}

}
