package com.awcsoftware.dto;

public class GeneralFolder extends RootFolder{
	private int parentFolderIndex;
	public GeneralFolder() {
	}
	public GeneralFolder(int parentFolderIndex) {
		super();
		this.parentFolderIndex = parentFolderIndex;
	}
	public int getParentFolderIndex() {
		return parentFolderIndex;
	}
	public void setParentFolderIndex(int parentFolderIndex) {
		this.parentFolderIndex = parentFolderIndex;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + parentFolderIndex;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneralFolder other = (GeneralFolder) obj;
		if (parentFolderIndex != other.parentFolderIndex)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "GeneralFolder [parentFolderIndex=" + parentFolderIndex + ", name=" + getName()
				+ ", folderIndex=" + getFolderIndex() + "]";
	}
		
}
