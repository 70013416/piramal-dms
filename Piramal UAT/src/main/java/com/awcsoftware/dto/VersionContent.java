/**
 * 
 */
package com.awcsoftware.dto;

public class VersionContent {
	private int documentId; 
	private float version;
	private String versionComment;
	private int imageIndex;
	public VersionContent() {
	}
	public VersionContent(int documentId, float version, String versionComment, int imageIndex) {
		super();
		this.documentId = documentId;
		this.version = version;
		this.versionComment = versionComment;
		this.imageIndex = imageIndex;
	}
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public float getVersion() {
		return version;
	}
	public void setVersion(float version) {
		this.version = version;
	}
	public String getVersionComment() {
		return versionComment;
	}
	public void setVersionComment(String versionComment) {
		this.versionComment = versionComment;
	}
	public int getImageIndex() {
		return imageIndex;
	}
	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + documentId;
		result = prime * result + imageIndex;
		result = prime * result + Float.floatToIntBits(version);
		result = prime * result + ((versionComment == null) ? 0 : versionComment.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionContent other = (VersionContent) obj;
		if (documentId != other.documentId)
			return false;
		if (imageIndex != other.imageIndex)
			return false;
		if (Float.floatToIntBits(version) != Float.floatToIntBits(other.version))
			return false;
		if (versionComment == null) {
			if (other.versionComment != null)
				return false;
		} else if (!versionComment.equals(other.versionComment))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VersionContent [documentId=" + documentId + ", version=" + version + ", versionComment="
				+ versionComment + ", imageIndex=" + imageIndex + "]";
	}
	
}
