package com.awcsoftware.dto;

public class MetaDataRequest {
    String leadId;
	String callerSystem;
	String updatedAfter;
	String updatedFlag;
	
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getCallerSystem() {
		return callerSystem;
	}
	public void setCallerSystem(String callerSystem) {
		this.callerSystem = callerSystem;
	}
	public String getUpdatedAfter() {
		return updatedAfter;
	}
	public void setUpdatedAfter(String updatedAfter) {
		this.updatedAfter = updatedAfter;
	}
	public String getUpdatedFlag() {
		return updatedFlag;
	}
	public void setUpdatedFlag(String updatedFlag) {
		this.updatedFlag = updatedFlag;
	}
	@Override
	public String toString() {
		return "MetaDataRequest [leadId=" + leadId + ", callerSystem=" + callerSystem + ", updatedAfter=" + updatedAfter
				+ ", updatedFlag=" + updatedFlag + "]";
	}
	
}
