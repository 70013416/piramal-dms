package com.awcsoftware.dto;

import java.util.Arrays;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BinaryData {
	private int imageIndex;
	private Byte[] data;
	private String error;
	public BinaryData() {
	}
	public BinaryData(int imageIndex, Byte[] data, String error) {
		super();
		this.imageIndex = imageIndex;
		this.data = data;
		this.error = error;
	}
	public int getImageIndex() {
		return imageIndex;
	}
	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}
	public Byte[] getData() {
		return data;
	}
	public void setData(Byte[] data) {
		this.data = data;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + imageIndex;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BinaryData other = (BinaryData) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (error == null) {
			if (other.error != null)
				return false;
		} else if (!error.equals(other.error))
			return false;
		if (imageIndex != other.imageIndex)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "BinaryData [imageIndex=" + imageIndex + ", data=" + Arrays.toString(data) + ", error=" + error + "]";
	}
	
}
