package com.awcsoftware.dto;

import java.util.Arrays;

public class BaseData {
	private String leadId,documentType,document,userName,docN;
	private byte[] binData;
	public BaseData() {
	}
	public BaseData(String leadId, String documentType, String document, String userName, String docN, byte[] binData) {
		super();
		this.leadId = leadId;
		this.documentType = documentType;
		this.document = document;
		this.userName = userName;
		this.docN = docN;
		this.binData = binData;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDocN() {
		return docN;
	}
	public void setDocN(String docN) {
		this.docN = docN;
	}
	public byte[] getBinData() {
		return binData;
	}
	public void setBinData(byte[] binData) {
		this.binData = binData;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(binData);
		result = prime * result + ((docN == null) ? 0 : docN.hashCode());
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((documentType == null) ? 0 : documentType.hashCode());
		result = prime * result + ((leadId == null) ? 0 : leadId.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseData other = (BaseData) obj;
		if (!Arrays.equals(binData, other.binData))
			return false;
		if (docN == null) {
			if (other.docN != null)
				return false;
		} else if (!docN.equals(other.docN))
			return false;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (documentType == null) {
			if (other.documentType != null)
				return false;
		} else if (!documentType.equals(other.documentType))
			return false;
		if (leadId == null) {
			if (other.leadId != null)
				return false;
		} else if (!leadId.equals(other.leadId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	/*@Override
	public String toString() {
		return "BaseData [leadId=" + leadId + ", documentType=" + documentType + ", document=" + document
				+ ", userName=" + userName + ", docN=" + docN + ", binData=" + Arrays.toString(binData) + "]";
	}*/
	@Override
	public String toString() {
		return "BaseData [leadId=" + leadId + ", documentType=" + documentType + ", document=" + document
				+ ", userName=" + userName + ", docN=" + docN + "]";
	}
	
	
}
