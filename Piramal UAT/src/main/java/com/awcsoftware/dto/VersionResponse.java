
package com.awcsoftware.dto;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VersionResponse {
	private int status;
	private int docId;
	private float verNo;
	private int documentIndex;
	private String comment;
	public VersionResponse() {
	}
	public VersionResponse(int status, int docId,float verNo,int documentIndex,String comment) {
		super();
		this.status = status;
		this.docId = docId;
		this.verNo = verNo;
		this.documentIndex=documentIndex;
		this.comment=comment;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	
	public float getVerNo() {
		return verNo;
	}
	public void setVerNo(float verNo) {
		this.verNo = verNo;
	}
	
	public int getDocumentIndex() {
		return documentIndex;
	}
	public void setDocumentIndex(int documentIndex) {
		this.documentIndex = documentIndex;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + docId;
		result = prime * result + documentIndex;
		result = prime * result + status;
		result = prime * result + Float.floatToIntBits(verNo);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionResponse other = (VersionResponse) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (docId != other.docId)
			return false;
		if (documentIndex != other.documentIndex)
			return false;
		if (status != other.status)
			return false;
		if (Float.floatToIntBits(verNo) != Float.floatToIntBits(other.verNo))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VersionResponse [status=" + status + ", docId=" + docId + ", verNo=" + verNo + ", documentIndex="
				+ documentIndex + ", comment=" + comment + "]";
	}
				
}
