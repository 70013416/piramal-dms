package com.awcsoftware.dto;

import java.util.Arrays;

public class CaseData extends BaseData {
	private String caseId;
	public CaseData() {
	}
	public CaseData(String leadId,String caseId, String documentType, String document, String userName, String docN, byte[] binData) {
		super(leadId, documentType, document, userName, docN, binData);
		this.caseId=caseId;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((caseId == null) ? 0 : caseId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CaseData other = (CaseData) obj;
		if (caseId == null) {
			if (other.caseId != null)
				return false;
		} else if (!caseId.equals(other.caseId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CaseData [caseId=" + caseId + ", getLeadId()=" + getLeadId() + ", getDocumentType()="
				+ getDocumentType() + ", getDocument()=" + getDocument() + ", getUserName()=" + getUserName()
				+ ", getDocN()=" + getDocN() + ", getBinData()=" + Arrays.toString(getBinData()) + "]";
	}
		
}
