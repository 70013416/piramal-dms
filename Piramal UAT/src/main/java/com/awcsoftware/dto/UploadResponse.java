/**
 * 
 */
package com.awcsoftware.dto;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Pratik
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UploadResponse {

	private int docId;
	private String status;
	private int imageIndex;
	public UploadResponse() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param docId
	 * @param status
	 */
	public UploadResponse(int docId, String status,int imageIndex) {
		super();
		this.docId = docId;
		this.status = status;
		this.imageIndex=imageIndex;
	}
	/**
	 * @return the docId
	 */
	public int getDocId() {
		return docId;
	}
	/**
	 * @param docId the docId to set
	 */
	public void setDocId(int docId) {
		this.docId = docId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the imageIndex
	 */
	public int getImageIndex() {
		return imageIndex;
	}
	/**
	 * @param imageIndex the imageIndex to set
	 */
	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + docId;
		result = prime * result + imageIndex;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadResponse other = (UploadResponse) obj;
		if (docId != other.docId)
			return false;
		if (imageIndex != other.imageIndex)
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UploadResponse [docId=" + docId + ", status=" + status + ", imageIndex=" + imageIndex + "]";
	}
	
}
