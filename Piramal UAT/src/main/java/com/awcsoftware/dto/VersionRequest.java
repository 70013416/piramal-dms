/**
 * 
 */
package com.awcsoftware.dto;

import java.util.Arrays;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VersionRequest {
	private int docId;
	private byte[] data;
	private String ext;
	private String comment;
	public VersionRequest() {
	}
	public VersionRequest(int docId, byte[] data, String ext, String comment) {
		super();
		this.docId = docId;
		this.data = data;
		this.ext = ext;
		this.comment = comment;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + docId;
		result = prime * result + ((ext == null) ? 0 : ext.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionRequest other = (VersionRequest) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (!Arrays.equals(data, other.data))
			return false;
		if (docId != other.docId)
			return false;
		if (ext == null) {
			if (other.ext != null)
				return false;
		} else if (!ext.equals(other.ext))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VersionRequest [docId=" + docId + ", data=" + Arrays.toString(data) + ", ext=" + ext + ", comment="
				+ comment + "]";
	}
				
}
