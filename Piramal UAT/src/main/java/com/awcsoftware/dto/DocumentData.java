/**
 * 
 */
package com.awcsoftware.dto;

import java.util.Arrays;

public class DocumentData extends BaseData{
	private String location,applicant,docSource,cif;
	public DocumentData() {
	}
	public DocumentData(String leadId, String documentType, String document, String userName, String docN,byte[] binData,String location,String applicant,String docSource) {
		super(leadId, documentType, document, userName, docN, binData);
		this.location=location;
		this.applicant=applicant;
		this.docSource=docSource;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getDocSource() {
		return docSource;
	}
	public void setDocSource(String docSource) {
		this.docSource = docSource;
	}
	
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	/*@Override
	public String toString() {
		return "DocumentData [location=" + location + ", applicant=" + applicant + ", docSource=" + docSource
				+ ", cif="+ cif + ", getLeadId()=" + getLeadId() + ", getDocumentType()=" + getDocumentType() + ", getDocument()="
				+ getDocument() + ", getUserName()=" + getUserName() + ", getDocN()=" + getDocN() + ", getBinData()="
				+ Arrays.toString(getBinData()) + "]";
	}*/
	@Override
	public String toString() {
		return "DocumentData [location=" + location + ", applicant=" + applicant + ", docSource=" + docSource + ", cif="
				+ cif + ", toString()=" + super.toString() + "]";
	}
	
	
}
