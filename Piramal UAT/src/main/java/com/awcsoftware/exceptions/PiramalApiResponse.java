package com.awcsoftware.exceptions;


import java.time.LocalDateTime;

public class PiramalApiResponse {
	LocalDateTime timestamp;
	String message;
	int status;

	public PiramalApiResponse() {
		// TODO Auto-generated constructor stub
	}	
	public PiramalApiResponse(LocalDateTime timestamp, String message) {
		super();
		this.timestamp = timestamp;
		this.message = message;
	}

	public PiramalApiResponse(LocalDateTime timestamp, String message, int status) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
}

