package com.awcsoftware.exceptions;


/**
 * @author S.M.Shuaib
 *
 */
public class PiramalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message = null;

	public PiramalException(String message) {
		super(message);
		this.message = message;
	}

	public PiramalException() {
		super();
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}
