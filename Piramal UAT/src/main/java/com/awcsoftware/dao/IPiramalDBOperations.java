package com.awcsoftware.dao;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.awcsoftware.dto.BaseData;
import com.awcsoftware.dto.ISIndexData;
import com.awcsoftware.dto.VersionContent;

public interface IPiramalDBOperations {
	public VersionContent getVersionData(int docId);
	public String validateSessionId(String sessionId);
	public String validateLocation(String location);
	public int validateCreateFolder(String locationData,BaseData dd, String sesisonId);
	public List<ISIndexData> getIsIndex(int docId) ;
	public List<Integer> getDocIdFromLeadId(String leadId);
	public List<Integer> getDocIdFromLeadIdFilterByDoctypes(String leadId, String[] props, Map<String, String> fieldMap);
	public String validatecallerSystem(String callerSystem);
	public boolean validateMimeType(File file);
}
