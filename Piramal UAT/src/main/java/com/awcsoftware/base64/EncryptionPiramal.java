/**
 * 
 */
package com.awcsoftware.base64;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Pratik
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EncryptionPiramal {
	
	public byte[] encodeData(byte[] data) {
		return DatatypeConverter.printBase64Binary(data).getBytes();
	}
	public byte[] decodeData(byte[] data) {	
		return DatatypeConverter.parseBase64Binary(new String(data));
	}
}
