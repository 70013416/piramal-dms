/**
 * 
 */
package com.awc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.awc.dbconnection.ConnectPiramalDB;
import com.awc.services.dmsapi.CabinetApi;
import com.newgen.dmsapi.DMSXmlResponse;

/**
 * @author Pratik
 *
 */
public class GetFolderFileDetails {
	private ConnectPiramalDB db;
	private Connection connection;
	private DMSXmlResponse xmlresponse;
	private CabinetApi api;
	private String sessionId=null;
	final static Logger logger = Logger.getLogger(GetFolderFileDetails.class);
	
	//Queries Start
	private String getDocumentDetailsQuery="select doc.Name,ddt.FoldDocIndex from PDBDocument doc,ddt_3 ddt where doc.DocumentIndex=ddt.FoldDocIndex and ddt.Field_16='N'";
	//Queries Closed
	
	
	private PreparedStatement documentDetailsPs;
	private ResultSet documentDetailsRs;
	
	public GetFolderFileDetails(){
		db = new ConnectPiramalDB();
		connection=db.getConnection();
		api=new CabinetApi();
		xmlresponse=new DMSXmlResponse();
		xmlresponse.setXmlString(api.connectCabinet());
		if (xmlresponse.getVal("Status").equalsIgnoreCase("0")) {
			sessionId = xmlresponse.getVal("UserDBId");
			logger.info("SessionId obtained :"+sessionId);
			
		}
		else {
			logger.error("Error Getting SessionId : "+xmlresponse.getVal("Error"));
			//System.err.println("Error Getting SessionId : "+xmlresponse.getVal("Error"));
		}
	}
	
	
	public Map<String,Integer> mapNames() {
		Map<String,Integer> documentDetailsMap=new LinkedHashMap<String,Integer>();
		try {
				documentDetailsPs=connection.prepareStatement(getDocumentDetailsQuery,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
				documentDetailsRs=documentDetailsPs.executeQuery();
				while(documentDetailsRs.next()) {
					documentDetailsMap.put(documentDetailsRs.getString(1), documentDetailsRs.getInt(2));
				}
			}
			catch(SQLException e) {
				logger.error(e);
				e.printStackTrace();
			}
			finally {
				try {
					documentDetailsRs.beforeFirst();
					if(documentDetailsRs.next()==false) {
						//System.out.println("Data In Result Set");
						logger.info("Data In Result Set");
						documentDetailsRs.close();
					}
					else {
						//System.out.println("NO-Data In Result Set");
						logger.info("NO-Data In Result Set");
					}
				}
				catch(SQLException e) {
					e.printStackTrace();
					logger.error(e);
				}
			}

		return documentDetailsMap;
	}
	
	
	public void splitMetaData() {
		
		try {
			logger.info("MetaDataSplit Method Start");
			Map<String,Integer> map=mapNames();
			if(map.isEmpty()==false) {
				System.out.println("Meta-Data Update Start");
				logger.info("Meta-Data Update Start");
				Set<Entry<String,Integer>> entrySet=map.entrySet();
				Iterator<Entry<String,Integer>> itr=entrySet.iterator();
				while(itr.hasNext()) {
						try {
							Map.Entry<String, Integer> mapEntry=itr.next();
							logger.info("Map Entry : "+mapEntry.getKey()+"\t ::-> "+mapEntry.getValue());
							String keyData=mapEntry.getKey();
							String data1=keyData.substring(0, keyData.indexOf(" "));
							//System.out.print("Data 1 : "+data1);
							logger.info("Data 1 : "+data1);
							keyData=keyData.substring(data1.length()+1);
							String data2=keyData.substring(0, keyData.indexOf(" "));
							//System.out.print(" : Data 2 : "+data2);
							//logger.info(" : Data 2 : "+data2);
							logger.info(" : Data 2 : "+data2);
							keyData=keyData.substring(data2.length()+1);
							//logger.info(" : KeyData Left : "+keyData);
							
							String data4=keyData.substring(keyData.lastIndexOf(" "));
							String data3=keyData.substring(0,keyData.length()- data4.length());
							//System.out.print(" : Data 3 : "+data3);
							logger.info(" : Data 3 : "+data3);
							//System.out.println(" : Data 4 : "+data4);
							logger.info(" : Data 4 : "+data4);
							
							String outputResponse=updateMetaData(mapEntry.getValue().toString(), mapEntry.getKey(), data1, data2, data3, data4);
							//System.out.println("Output Response"+outputResponse);
							logger.info("Output Response"+outputResponse);
							
						}
						catch(StringIndexOutOfBoundsException e) {
							e.printStackTrace();
						}
					}
			}
			else {
				//System.out.println("Data Already Updated In Submit Folder !!");
				logger.info("Data Already Updated In Submit Folder !!");
			}
		}
		finally {
			api.disconnectCabinet(sessionId);
		}
	}
	
	
	public String updateMetaData(String docId,String docName,String data1,String data2,String data3,String data4) {
		
		String inputXml="<?xml version=\"1.0\"?>\r\n" + 
				"<NGOChangeDocumentProperty_Input>\r\n" + 
				"    <Option>NGOChangeDocumentProperty</Option>\r\n" + 
				"    <CabinetName>piramal</CabinetName>\r\n" + 
				"    <UserDBId>"+ sessionId +"</UserDBId>\r\n" + 
				"    <GroupIndex>0</GroupIndex>\r\n" + 
				"    <Document>\r\n" + 
				"        <DocumentIndex>"+docId+"</DocumentIndex>\r\n" + 
				"        <DocumentName>"+docName+"</DocumentName>\r\n" + 
				"        <Owner>Supervisor</Owner>\r\n" + 
				"        <VersionFlag></VersionFlag>\r\n" + 
				"        <Comment></Comment>\r\n" + 
				"        <DataDefinition>\r\n" + 
				"            <DataDefName>document</DataDefName>\r\n" + 
				"            <Fields>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>9</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>"+data1+"</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>10</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>"+data2+"</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>11</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>"+data3+"</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>12</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>"+data4+"</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"                <Field>\r\n" + 
				"                    <IndexId>16</IndexId>\r\n" + 
				"                    <IndexType>S</IndexType>\r\n" + 
				"                    <IndexValue>Y</IndexValue>\r\n" + 
				"                </Field>\r\n" + 
				"            </Fields>\r\n" + 
				"        </DataDefinition>\r\n" + 
				"        <OwnerIndex></OwnerIndex>\r\n" + 
				"        <OwnerType>U</OwnerType>\r\n" + 
				"    </Document>\r\n" + 
				"</NGOChangeDocumentProperty_Input>";
		
		return api.callBroker(inputXml);
	}
	
	public static void main(String[] args) {
		logger.info("MetaData Update Starting at :"+new Date());
		GetFolderFileDetails gfd=new GetFolderFileDetails();
		gfd.splitMetaData();
		gfd=null;
		logger.info("MetaData Update Ending at :"+new Date());
	}
}
